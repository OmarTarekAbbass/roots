<?php
if (isset($_POST['b_reg'])) {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $subject = $_POST['subject'];
    $message = $_POST['message'];
    $_SESSION['flash'] = 'Massage Has Been Sent!';

     //echo $name .'<br/>';
     //echo $email .'<br/>';
     //echo $subject .'<br/>';
     //echo $message .'<br/>';
    $message = $_POST['name'] . ' :الاسم' . "\r\n"
        . $_POST['email'] . ' :البريد الإلكتروني' . "\r\n"
        . $_POST['subject'] . ' :العنوان' . "\r\n"
        . $_POST['message'] . ' :الرسالة' . "\r\n";
    $headers = "From: " . $_POST['email'];
    @mail('omer.tarek@ivas.com.eg', 'Roots', $message, $headers);
}


?>

<!doctype html>
<html class="no-js" lang="en-US">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <title>Roots</title>
    <link rel="icon" href="image/roots_home.png" type="image/x-icon"> <!-- Favicon-->
    <meta property="fb:pages" content="266103416770143" />
    <meta property="og:title" content="Roots">
    <meta property="og:description" content="To help brands penetrate new markets, empower existing markets, and compete at the highest level, we offer full integrated creative solutions set. We treat visual communications as one of the most effective and engaging techniques, we oversee all aspects that help customers to understand the brand better.">
    <meta property="og:image" content="image/roots_home.png">
    <meta property="og:url" content="#">
    <meta name="description" content="Roots - Digital Marketing & Video Production"/>
    <meta name="keywords"
          content="To help brands penetrate new markets, empower existing markets, and compete at the highest level, we offer full integrated creative solutions set. We treat visual communications as one of the most effective and engaging techniques, we oversee all aspects that help customers to understand the brand better."/>
    <meta name="author" content="beingeorge"/>
    <meta name="theme-color" content="#2a2d35">

    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />

    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/",
            "svgExt": ".svg",
            "source": {
                "concatemoji": "https:\/\/oshinewptheme.com\/v14\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.4"
            }
        };
        ! function(e, a, t) {
            var r, n, o, i, p = a.createElement("canvas"),
                s = p.getContext && p.getContext("2d");

            function c(e, t) {
                var a = String.fromCharCode;
                s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, e), 0, 0);
                var r = p.toDataURL();
                return s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, t), 0, 0), r === p.toDataURL()
            }

            function l(e) {
                if (!s || !s.fillText) return !1;
                switch (s.textBaseline = "top", s.font = "600 32px Arial", e) {
                    case "flag":
                        return !c([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) && (!c([55356,
                            56826, 55356, 56819
                        ], [55356, 56826, 8203, 55356, 56819]) && !c([55356, 57332, 56128, 56423, 56128, 56418,
                            56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447
                        ], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203,
                            56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447
                        ]));
                    case "emoji":
                        return !c([55357, 56424, 55356, 57342, 8205, 55358, 56605, 8205, 55357, 56424, 55356, 57340], [
                            55357, 56424, 55356, 57342, 8203, 55358, 56605, 8203, 55357, 56424, 55356, 57340
                        ])
                }
                return !1
            }

            function d(e) {
                var t = a.createElement("script");
                t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t)
            }

            for (i = Array("flag", "emoji"), t.supports = {
                    everything: !0,
                    everythingExceptFlag: !0
                }, o = 0; o < i.length; o++) t.supports[i[o]] = l(i[o]), t.supports.everything = t.supports.everything && t
                .supports[i[o]], "flag" !== i[o] && (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t
                    .supports[i[o]]);
            t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t
                .readyCallback = function() {
                    t.DOMReady = !0
                }, t.supports.everything || (n = function() {
                    t.readyCallback()
                }, a.addEventListener ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !
                    1)) : (e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function() {
                    "complete" === a.readyState && t.readyCallback()
                })), (r = t.source || {}).concatemoji ? d(r.concatemoji) : r.wpemoji && r.twemoji && (d(r.twemoji), d(r
                    .wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='wp-block-library-css' href='css/style.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='oshine-modules-css' href='css/oshine-modules.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='rs-plugin-settings-css' href='css/rs6.css' type='text/css' media='all' />
    <style id='rs-plugin-settings-inline-css' type='text/css'>
        #rs-demo-id {}
    </style>
    <link rel='stylesheet' id='tatsu-main-css-css' href='css/tatsu.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='oshine_icons-css' href='css/style2.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='be-themes-bb-press-css-css' href='css/bb-press.css' type='text/css' media='all' />
    <link rel='stylesheet' id='be-style-main-css-css' href='css/main.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='be-style-top-header-css' href='css/top-header.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='be-style-responsive-header-css' href='css/responsive-header.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='be-style-multilevel-menu-css' href='css/multilevel-menu.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='be-themes-layout-css' href='css/layout.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='vendor-css' href='css/vendor.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='be-custom-fonts-css' href='css/fonts.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='be-style-css-css' href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='ms-main-css' href='css/masterslider.main.css' type='text/css' media='all' />
    <link rel='stylesheet' id='ms-custom-css' href='css/custom.css' type='text/css' media='all' />
    <link rel='stylesheet' id='redux-google-fonts-be_themes_data-css' href='https://fonts.googleapis.com/css?family=Montserrat%3A700%2C400%7CRaleway%3A400%2C300%2C600%7CCrimson+Text%3A400italic%2C400Italic&#038;subset=latin&#038;ver=1575567614' type='text/css' media='all' />

    <script type='text/javascript' src='js/jquery.js'>
    </script>
    <script type='text/javascript' src='js/jquery-migrate.min.js'>
    </script>
    <script type='text/javascript' src='js/revolution.tools.min.js'>
    </script>
    <script type='text/javascript' src='js/rs6.min.js'>
    </script>
    <script type='text/javascript' src='js/modernizr.min.js'>
    </script>
    <script type='text/javascript' src='js/webfont.min.js'>
    </script>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <meta name="generator" content="MasterSlider 3.2.14 - Responsive Touch Image Slider" />
    <!-- Begin Inspectlet Async Code -->
    <script type="text/javascript">
        (function() {
            window.__insp = window.__insp || [];
            __insp.push(['wid', 5763783]);
            var ldinsp = function() {
                if (typeof window.__inspld != "undefined") return;
                window.__inspld = 1;
                var insp = document.createElement('script');
                insp.type = 'text/javascript';
                insp.async = true;
                insp.id = "inspsync";
                insp.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                    '://cdn.inspectlet.com/inspectlet.js?wid=5763783&r=' + Math.floor(new Date().getTime() /
                        3600000);
                var x = document.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(insp, x);
            };
            setTimeout(ldinsp, 0);
        })();
    </script>
    <!-- End Inspectlet Async Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '3314662211884561');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=3314662211884561&ev=PageView&noscript=1" /></noscript>

    <link rel='stylesheet' id='oshine_icons-css' href='css/css_home.css' type='text/css' media='all' />
    <link rel='stylesheet' id='oshine_icons-css' href='https://21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com/v14/wp-content/themes/oshin/fonts/icomoon/style.min.css?ver=6.8.5' type='text/css' media='all' />


    <meta name="generator" content="Powered by Slider Revolution 6.1.3 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
    <style id="oshine-typehub-css" type="text/css"></style>
    <script type="text/javascript">
        function setREVStartSize(t) {
            try {
                var h, e = document.getElementById(t.c).parentNode.offsetWidth;
                if (e = 0 === e || isNaN(e) ? window.innerWidth : e, t.tabw = void 0 === t.tabw ? 0 : parseInt(t.tabw), t
                    .thumbw = void 0 === t.thumbw ? 0 : parseInt(t.thumbw), t.tabh = void 0 === t.tabh ? 0 : parseInt(t
                        .tabh), t.thumbh = void 0 === t.thumbh ? 0 : parseInt(t.thumbh), t.tabhide = void 0 === t.tabhide ?
                    0 : parseInt(t.tabhide), t.thumbhide = void 0 === t.thumbhide ? 0 : parseInt(t.thumbhide), t.mh =
                    void 0 === t.mh || "" == t.mh || "auto" === t.mh ? 0 : parseInt(t.mh, 0), "fullscreen" === t.layout ||
                    "fullscreen" === t.l) h = Math.max(t.mh, window.innerHeight);
                else {
                    for (var i in t.gw = Array.isArray(t.gw) ? t.gw : [t.gw], t.rl) void 0 !== t.gw[i] && 0 !== t.gw[i] || (
                        t.gw[i] = t.gw[i - 1]);
                    for (var i in t.gh = void 0 === t.el || "" === t.el || Array.isArray(t.el) && 0 == t.el.length ? t.gh :
                            t.el, t.gh = Array.isArray(t.gh) ? t.gh : [t.gh], t.rl) void 0 !== t.gh[i] && 0 !== t.gh[i] || (
                        t.gh[i] = t.gh[i - 1]);
                    var r, a = new Array(t.rl.length),
                        n = 0;
                    for (var i in t.tabw = t.tabhide >= e ? 0 : t.tabw, t.thumbw = t.thumbhide >= e ? 0 : t.thumbw, t.tabh =
                            t.tabhide >= e ? 0 : t.tabh, t.thumbh = t.thumbhide >= e ? 0 : t.thumbh, t.rl) a[i] = t.rl[i] <
                        window.innerWidth ? 0 : t.rl[i];
                    for (var i in r = a[0], a) r > a[i] && 0 < a[i] && (r = a[i], n = i);
                    var d = e > t.gw[n] + t.tabw + t.thumbw ? 1 : (e - (t.tabw + t.thumbw)) / t.gw[n];
                    h = t.gh[n] * d + (t.tabh + t.thumbh)
                }
                void 0 === window.rs_init_css && (window.rs_init_css = document.head.appendChild(document.createElement(
                        "style"))), document.getElementById(t.c).height = h, window.rs_init_css.innerHTML += "#" + t.c +
                    "_wrapper { height: " + h + "px }"
            } catch (t) {
                console.log("Failure at Presize of Slider:" + t)
            }
        };
    </script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-969580894"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'AW-969580894');
    </script>
</head>
<style>
    #header-inner-wrap.transparent:not(.no-transparent).background--light .logo .dark-scheme-logo {
        width: 21%;
        position: absolute;
        top: -41px;
        left: -4%;
    }

    .top-animate .logo .sticky-logo {
        width: 16%;
        position: absolute;
        top: -41px;
        left: -4%;
    }

    @media only screen and (min-width: 0) and (max-width: 600px) {
        #header-inner-wrap.transparent:not(.no-transparent).background--light .logo .dark-scheme-logo {
            width: 57%;
            position: absolute;
            top: -30px;
            left: -14%;
        }
    }
    .contact_form_3{
        color: #71ff43;
        text-align: center;
    }
</style>

<body class="home page-template-default page page-id-3338 _masterslider _msp_version_3.2.14 transparent-sticky header-transparent no-section-scroll single-page-version top-header top-right-sliding-menu be-themes-layout-layout-wide disable_rev_slider_bg_check opt-panel-cache-off" data-be-site-layout='layout-wide' data-be-page-template='page'>
    <div class="search-box-wrapper style2-header-search-widget">
        <a href="#" class="header-search-form-close"><i class="icon-icon_close font-icon"></i></a>
        <div class="search-box-inner1">

        </div>
    </div>
    <div id="main-wrapper">
        <div id="main" class="ajaxable layout-wide">

            <header id="header">
                <div id="header-inner-wrap" class="transparent background--light style1" data-headerscheme="background--light">
                    <div id="header-wrap" class="be-wrap clearfix" data-default-height="100" data-sticky-height="100">
                        <div class="logo">
                            <a href="index.php"><img class="transparent-logo dark-scheme-logo" src="image/roots.png" alt="Roots" /><img class="transparent-logo light-scheme-logo" src="image/roots.png" alt="Roots" /><img class="normal-logo" src="image/roots.png" alt="Roots" /><img class="sticky-logo" src="image/roots.png" alt="Roots" /></a></div>
                        <div id="header-controls-right">

                            <div class="mobile-nav-controller-wrap">
                                <div class="menu-controls mobile-nav-controller" title="Mobile Menu Controller"> <span class="be-mobile-menu-icon style1">
                                        <span class="hamburger-line-1"></span>
                                        <span class="hamburger-line-2"></span>
                                        <span class="hamburger-line-3"></span>
                                    </span></div>
                            </div>
                        </div>
                        <nav id="navigation" class="clearfix">
                            <div class="menu">
                                <ul id="menu" class="clearfix none">
                                    <li id="menu-item-3339" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3339">
                                        <a title="Home" href="#header">Home</a></li>
                                    <li id="menu-item-3340" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3340">
                                        <a title="About" href="#about" aria-current="page">About</a>
                                    </li>
                                    <li id="menu-item-3341" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3341">
                                        <a title="Team" href="#team" aria-current="page">Team</a></li>
                                    <li id="menu-item-3342" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3342">
                                        <a title="Services" href="#services" aria-current="page">Services</a></li>
                                    <li id="menu-item-3343" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3343">
                                        <a title="Work" href="#portfolio" aria-current="page">Work</a>
                                    </li>
                                    <!--<li id="menu-item-3346" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3346">
                                        <a title="Pricing" href="#pricing" aria-current="page">Pricing</a></li>-->
                                    <!--<li id="menu-item-3344" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3344">
                                        <a title="Clients" href="#clients" aria-current="page">Clients</a></li>-->
                                    <li id="menu-item-3345" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3345">
                                        <a title="Contact" href="#contact" aria-current="page">Contact</a></li>
                                </ul>
                            </div>
                        </nav><!-- End Navigation -->
                    </div>

                    <div class='header-mobile-navigation clearfix'>
                        <div class="mobile-menu">
                            <ul id="mobile-menu" class="clearfix">
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3339"><a title="Home" href="https://oshinewptheme.com/#header">Home</a></li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3340">
                                    <a title="About" href="#about" aria-current="page">About</a></li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3341">
                                    <a title="Team" href="#team" aria-current="page">Team</a>
                                </li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3342">
                                    <a title="Services" href="#services" aria-current="page">Services</a>
                                </li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3343">
                                    <a title="Work" href="#portfolio" aria-current="page">Work</a></li>
                               <!-- <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3346">
                                    <a title="Pricing" href="#pricing" aria-current="page">Pricing</a>
                                </li>-->
                                <!--<li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3344">
                                    <a title="Clients" href="#clients" aria-current="page">Clients</a>
                                </li>-->
                                <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3345">
                                    <a title="Contact" href="#contact" aria-current="page">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header> <!-- END HEADER -->
            <div class="header-hero-section" id="hero-section">
                <!-- START v14home REVOLUTION SLIDER 6.1.3 -->
                <p class="rs-p-wp-fix"></p>
                <rs-module-wrap id="rev_slider_3_1_wrapper" data-source="gallery" style="background:#E9E9E9;padding:0;">
                    <rs-module id="rev_slider_3_1" style="display:none;" data-version="6.1.3">
                        <rs-slides>
                            <rs-slide data-key="rs-8" data-title="Slide" data-thumb="image/stage-bg.jpg" data-anim="ei:d;eo:d;s:800;r:0;t:parallaxtoleft;sl:7;">
                                <img src="image/stage-bg.jpg" title="Home- Single-new" data-parallax="1" class="rev-slidebg" data-no-retina>
                                <!--
                                                        -->
                                <rs-layer id="slider-3-slide-8-layer-1" class="rs-pxl-3" data-type="image" data-rsp_ch="on" data-xy="x:c;xo:1px,1px,0,3px;y:b;yo:-6px,-6px,-8px,-2px;" data-text="l:22;" data-dim="w:680px,680px,603px,367px;h:300px,300px,266px,162px;" data-frame_0="y:bottom;o:1;tp:600;" data-frame_1="tp:600;st:600;sp:1200;sR:600;" data-frame_999="st:w;auto:true;" style="z-index:5;">
                                    <img src="image/mbp-new.png" data-no-retina>
                                </rs-layer>
                                <!--

							-->
                                <rs-layer id="slider-3-slide-8-layer-2" class="rs-pxl-10" data-type="image" data-rsp_ch="on" data-xy="xo:235px,235px,-25px,5px;yo:330px,330px,507px,58px;" data-text="l:22;" data-dim="w:256px,256px,193px,96px;h:227px,227px,171px,85px;" data-vbility="t,t,f,f" data-layeronlimit="on" data-frame_0="x:left;o:1;tp:600;" data-frame_1="tp:600;st:700;sp:1300;sR:700;" data-frame_999="st:w;auto:true;" style="z-index:6;">
                                    <img src="image/brushbox-new.png" data-no-retina>
                                </rs-layer>
                                <!--

							-->
                                <rs-layer id="slider-3-slide-8-layer-3" class="rs-pxl-6" data-type="image" data-rsp_ch="on" data-xy="xo:329px,329px,38px,3px;y:b;yo:84px,84px,-38px,-8px;" data-text="l:22;" data-dim="w:158px,158px,133px,91px;h:240px,240px,202px,138px;" data-layeronlimit="on" data-frame_0="x:left;o:1;tp:600;" data-frame_1="tp:600;st:800;sp:1500;sR:800;" data-frame_999="st:w;auto:true;" style="z-index:7;">
                                    <img src="image/iphone-new.png" data-no-retina>
                                </rs-layer>
                                <!--

							-->
                                <rs-layer id="slider-3-slide-8-layer-4" class="rs-pxl-7" data-type="image" data-rsp_ch="on" data-xy="xo:1374px,1374px,684px,450px;y:b;yo:21px,21px,-1px,-3px;" data-text="l:22;" data-dim="w:86px,86px,52px,52px;h:143px,143px,87px,86px;" data-layeronlimit="on" data-frame_0="y:bottom;o:1;tp:600;" data-frame_1="tp:600;st:800;sp:1200;sR:800;" data-frame_999="st:w;auto:true;" style="z-index:8;">
                                    <img src="image/apple-headphones.png" data-no-retina>
                                </rs-layer>
                                <!--

							-->
                                <rs-layer id="slider-3-slide-8-layer-5" class="rs-pxl-7" data-type="image" data-rsp_ch="on" data-xy="x:r;xo:-1px,-1px,-149px,-1px;y:b;yo:196px,196px,224px,196px;" data-text="l:22;" data-dim="w:455px,455px,226px,472;h:557px,557px,277px,578;" data-vbility="t,t,f,f" data-layeronlimit="on" data-frame_0="x:right;o:1;tp:600;" data-frame_1="tp:600;st:900;sp:1500;sR:900;" data-frame_999="st:w;auto:true;" style="z-index:9;">
                                    <img src="image/pad-new.png" data-no-retina>
                                </rs-layer>
                                <!--

							-->
                                <rs-layer id="slider-3-slide-8-layer-6" class="rs-pxl-3" data-type="image" data-xy="x:c;xo:589px,589px,391px,248px;y:m;yo:411px,411px,356px,247px;" data-text="l:22;" data-dim="w:230px,230px,151px,118px;h:223px,223px,146px,114px;" data-vbility="t,t,f,f" data-rsp_bd="off" data-frame_0="x:right;o:1;tp:600;" data-frame_1="tp:600;st:900;sp:1800;sR:900;" data-frame_999="st:w;auto:true;" style="z-index:10;">
                                    <img src="image/coffee-cup.png" data-no-retina>
                                </rs-layer>
                                <!--

							-->
                                <rs-layer id="slider-3-slide-8-layer-7" class="oshin-title-black" data-type="text" data-color="rgba(0, 0, 0, 1)" data-rsp_ch="on" data-xy="x:c;xo:-2px;y:m;yo:-150px;" data-text="s:55,55,55,30;fw:700;" data-frame_0="y:-50px;tp:600;" data-frame_1="tp:600;st:1500;sp:1200;sR:1500;" data-frame_999="st:w;auto:true;" style="z-index:11;font-family:Montserrat;text-shadow:none;">WELCOME TO ROOTS
                                </rs-layer>
                                <!--

							-->
                                <rs-layer id="slider-3-slide-8-layer-8" class="oshin-caption-black" data-type="text" data-color="rgba(54, 54, 54, 1)" data-rsp_ch="on" data-xy="x:c;xo:-4px;y:m;yo:-55px;" data-text="w:normal;s:20,20,20,16;l:39;fw:300;a:center;" data-dim="w:500px,500px,500px,482px;h:auto,auto,auto,79px;" data-frame_0="y:-50px;tp:600;" data-frame_1="tp:600;st:1600;sp:1200;sR:1600;" data-frame_999="st:w;auto:true;" style="z-index:12;font-family:Raleway;text-shadow:none;text-align:center;">
                                    Talk with experienced experts that listen to what you want and design a strategy to help you get there.
                                </rs-layer>
                                <!--

							-->

                                <!--
-->
                            </rs-slide>
                            <rs-slide data-key="rs-10" data-title="Slide" data-thumb="image/Canva_home.jpg" data-anim="ei:d;eo:d;s:800;r:0;t:parallaxtoright;sl:7;">
                                <img src="image/Canva_home.jpg" title="Home- Single-new" data-parallax="1" class="rev-slidebg" data-no-retina>
                                <!--
                                                        -->
                                <rs-layer id="slider-3-slide-10-layer-1" class="oshin-title" data-type="text" data-color="rgba(255, 255, 255, 1)" data-rsp_ch="on" data-xy="x:c;xo:-5px,-5px,-5px,0;y:m;yo:-226px;" data-text="s:55,55,55,45;fw:700;" data-frame_0="y:-50px;tp:600;" data-frame_1="tp:600;st:600;sp:1200;sR:600;" data-frame_999="st:w;auto:true;" style="z-index:5;font-family:Montserrat;text-shadow:none;">
                                <h4 style="color: white;">DIGITAL MARKETING SERVICES</h4> 
                                </rs-layer>
                                <!--

							-->
                                <rs-layer id="slider-3-slide-10-layer-2" class="oshin-caption" data-type="text" data-color="rgba(255, 255, 255, 1)" data-rsp_ch="on" data-xy="x:c;xo:-3px;y:m;yo:-121px;" data-text="w:normal;s:22;l:39;fw:300;a:center;" data-dim="w:550px,550px,550px,100%;h:78px;" data-basealign="slide" data-padding="r:0,0,0,20;l:0,0,0,20;" data-frame_0="y:50px;tp:600;" data-frame_1="tp:600;st:800;sp:1200;sR:800;" data-frame_999="st:w;auto:true;" style="z-index:6;font-family:Raleway;text-shadow:none;text-align:center;">We’re passionate about delivering quality and effective online marketing solutions with amazing results
                                </rs-layer>
                                <!--

							-->

                                <!--
-->
                            </rs-slide>
                        </rs-slides>
                        <rs-progress style="height: 5px; background: rgba(0,0,0,0.15);"></rs-progress>
                    </rs-module>
                    <script type="text/javascript">
                        setREVStartSize({
                            c: 'rev_slider_3_1',
                            rl: [1240, 1240, 778, 480],
                            el: [],
                            gw: [1920, 1920, 778, 480],
                            gh: [980, 980, 960, 720],
                            layout: 'fullscreen',
                            offsetContainer: '',
                            offset: '',
                            mh: "0"
                        });
                        var revapi3,
                            tpj;
                        jQuery(function() {
                            tpj = jQuery;
                            tpj.noConflict();
                            if (tpj("#rev_slider_3_1").revolution == undefined) {
                                revslider_showDoubleJqueryError("#rev_slider_3_1");
                            } else {
                                revapi3 = tpj("#rev_slider_3_1").show().revolution({
                                    jsFileLocation: "//oshinewptheme.com/v14/wp-content/plugins/revslider/public/assets/js/",
                                    sliderLayout: "fullscreen",
                                    visibilityLevels: "1240,1240,778,480",
                                    gridwidth: "1920,1920,778,480",
                                    gridheight: "980,980,960,720",
                                    minHeight: "",
                                    spinner: "spinner2",
                                    responsiveLevels: "1240,1240,778,480",
                                    gridEQModule: true,
                                    navigation: {
                                        mouseScrollNavigation: false,
                                        touch: {
                                            touchenabled: true
                                        },
                                        arrows: {
                                            enable: true,
                                            tmp: "<div class=\"tp-arr-allwrapper\">	<div class=\"tp-arr-imgholder\"></div>	<div class=\"tp-arr-titleholder\">{{title}}</div>	</div>",
                                            style: "hermes",
                                            hide_onmobile: true,
                                            hide_under: 767,
                                            left: {
                                                h_offset: 30
                                            },
                                            right: {
                                                h_offset: 50
                                            }
                                        },
                                        bullets: {
                                            enable: true,
                                            tmp: "<span class=\"tp-bullet-image\"></span>",
                                            style: "hades"
                                        }
                                    },
                                    parallax: {
                                        levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 47, 48, 49, 50, 51,
                                            55
                                        ],
                                        type: "scroll"
                                    },
                                    fallbacks: {
                                        allowHTML5AutoPlayOnAndroid: true
                                    },
                                });
                            }

                        });
                    </script>
                    <script>
                        var htmlDivCss =
                            ' #rev_slider_3_1_wrapper rs-loader.spinner2{ background-color: #38d2d9 !important; } '
                        var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                        if (htmlDiv) {
                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                        } else {
                            var htmlDiv = document.createElement('div');
                            htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                            document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                        }
                    </script>
                    <script>
                        var htmlDivCss = unescape(
                            "%23rev_slider_3_1_wrapper%20.hermes.tparrows%20%7B%0A%09cursor%3Apointer%3B%0A%09background%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%09width%3A30px%3B%0A%09height%3A110px%3B%0A%09position%3Aabsolute%3B%0A%09display%3Ablock%3B%0A%09z-index%3A1000%3B%0A%7D%0A%0A%23rev_slider_3_1_wrapper%20.hermes.tparrows%3Abefore%20%7B%0A%09font-family%3A%20%27revicons%27%3B%0A%09font-size%3A15px%3B%0A%09color%3A%23ffffff%3B%0A%09display%3Ablock%3B%0A%09line-height%3A%20110px%3B%0A%09text-align%3A%20center%3B%0A%20%20%20%20transform%3Atranslatex%280px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%280px%29%3B%0A%20%20%20%20transition%3Aall%200.3s%3B%0A%20%20%20%20-webkit-transition%3Aall%200.3s%3B%0A%7D%0A%23rev_slider_3_1_wrapper%20.hermes.tparrows.tp-leftarrow%3Abefore%20%7B%0A%09content%3A%20%27%5Ce824%27%3B%0A%7D%0A%23rev_slider_3_1_wrapper%20.hermes.tparrows.tp-rightarrow%3Abefore%20%7B%0A%09content%3A%20%27%5Ce825%27%3B%0A%7D%0A%23rev_slider_3_1_wrapper%20.hermes.tparrows.tp-leftarrow%3Ahover%3Abefore%20%7B%0A%20%20%20%20transform%3Atranslatex%28-20px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%28-20px%29%3B%0A%20%20%20%20%20opacity%3A0%3B%0A%7D%0A%23rev_slider_3_1_wrapper%20.hermes.tparrows.tp-rightarrow%3Ahover%3Abefore%20%7B%0A%20%20%20%20transform%3Atranslatex%2820px%29%3B%0A%20%20%20%20-webkit-transform%3Atranslatex%2820px%29%3B%0A%20%20%20%20%20opacity%3A0%3B%0A%7D%0A%0A%23rev_slider_3_1_wrapper%20.hermes%20.tp-arr-allwrapper%20%7B%0A%20%20%20%20overflow%3Ahidden%3B%0A%20%20%20%20position%3Aabsolute%3B%0A%09width%3A180px%3B%0A%20%20%20%20height%3A140px%3B%0A%20%20%20%20top%3A0px%3B%0A%20%20%20%20left%3A0px%3B%0A%20%20%20%20visibility%3Ahidden%3B%0A%20%20%20%20%20%20-webkit-transition%3A%20-webkit-transform%200.3s%200.3s%3B%0A%20%20transition%3A%20transform%200.3s%200.3s%3B%0A%20%20-webkit-perspective%3A%201000px%3B%0A%20%20perspective%3A%201000px%3B%0A%20%20%20%20%7D%0A%23rev_slider_3_1_wrapper%20.hermes.tp-rightarrow%20.tp-arr-allwrapper%20%7B%0A%20%20%20right%3A0px%3Bleft%3Aauto%3B%0A%20%20%20%20%20%20%7D%0A%23rev_slider_3_1_wrapper%20.hermes.tparrows%3Ahover%20.tp-arr-allwrapper%20%7B%0A%20%20%20visibility%3Avisible%3B%0A%20%20%20%20%20%20%20%20%20%20%7D%0A%23rev_slider_3_1_wrapper%20.hermes%20.tp-arr-imgholder%20%7B%0A%20%20width%3A180px%3Bposition%3Aabsolute%3B%0A%20%20left%3A0px%3Btop%3A0px%3Bheight%3A110px%3B%0A%20%20transform%3Atranslatex%28-180px%29%3B%0A%20%20-webkit-transform%3Atranslatex%28-180px%29%3B%0A%20%20transition%3Aall%200.3s%3B%0A%20%20transition-delay%3A0.3s%3B%0A%7D%0A%23rev_slider_3_1_wrapper%20.hermes.tp-rightarrow%20.tp-arr-imgholder%7B%0A%20%20%20%20transform%3Atranslatex%28180px%29%3B%0A%20%20-webkit-transform%3Atranslatex%28180px%29%3B%0A%20%20%20%20%20%20%7D%0A%20%20%0A%23rev_slider_3_1_wrapper%20.hermes.tparrows%3Ahover%20.tp-arr-imgholder%20%7B%0A%20%20%20transform%3Atranslatex%280px%29%3B%0A%20%20%20-webkit-transform%3Atranslatex%280px%29%3B%20%20%20%20%20%20%20%20%20%20%20%20%0A%7D%0A%23rev_slider_3_1_wrapper%20.hermes%20.tp-arr-titleholder%20%7B%0A%20%20top%3A110px%3B%0A%20%20width%3A180px%3B%0A%20%20text-align%3Aleft%3B%20%0A%20%20display%3Ablock%3B%0A%20%20padding%3A0px%2010px%3B%0A%20%20line-height%3A30px%3B%20background%3A%23000%3B%0A%20%20background%3Argba%280%2C0%2C0%2C0.75%29%3B%0A%20%20color%3A%23ffffff%3B%0A%20%20font-weight%3A600%3B%20position%3Aabsolute%3B%0A%20%20font-size%3A12px%3B%0A%20%20white-space%3Anowrap%3B%0A%20%20letter-spacing%3A1px%3B%0A%20%20-webkit-transition%3A%20all%200.3s%3B%0A%20%20transition%3A%20all%200.3s%3B%0A%20%20-webkit-transform%3A%20rotatex%28-90deg%29%3B%0A%20%20transform%3A%20rotatex%28-90deg%29%3B%0A%20%20-webkit-transform-origin%3A%2050%25%200%3B%0A%20%20transform-origin%3A%2050%25%200%3B%0A%20%20box-sizing%3Aborder-box%3B%0A%0A%7D%0A%23rev_slider_3_1_wrapper%20.hermes.tparrows%3Ahover%20.tp-arr-titleholder%20%7B%0A%20%20%20%20-webkit-transition-delay%3A%200.6s%3B%0A%20%20transition-delay%3A%200.6s%3B%0A%20%20-webkit-transform%3A%20rotatex%280deg%29%3B%0A%20%20transform%3A%20rotatex%280deg%29%3B%0A%7D%0A%0A%23rev_slider_3_1_wrapper%20.hades.tp-bullets%20%7B%0A%7D%0A%23rev_slider_3_1_wrapper%20.hades.tp-bullets%3Abefore%20%7B%0A%09content%3A%27%20%27%3B%0A%09position%3Aabsolute%3B%0A%09width%3A100%25%3B%0A%09height%3A100%25%3B%0A%09background%3Atransparent%3B%0A%09padding%3A10px%3B%0A%09margin-left%3A-10px%3Bmargin-top%3A-10px%3B%0A%09box-sizing%3Acontent-box%3B%0A%7D%0A%23rev_slider_3_1_wrapper%20.hades%20.tp-bullet%20%7B%0A%09width%3A3px%3B%0A%09height%3A3px%3B%0A%09position%3Aabsolute%3B%0A%09background%3A%237f7f7f%3B%0A%09cursor%3A%20pointer%3B%0A%20%20%20%20border%3A5px%20solid%20%23ffffff%3B%0A%09box-sizing%3Acontent-box%3B%0A%20%20%20%20box-shadow%3A0px%200px%203px%201px%20rgba%280%2C0%2C0%2C0.2%29%3B%0A%20%20%20%20-webkit-perspective%3A400%3B%0A%20%20%20%20perspective%3A400%3B%0A%20%20%20%20-webkit-transform%3Atranslatez%280.01px%29%3B%0A%20%20%20%20transform%3Atranslatez%280.01px%29%3B%0A%7D%0A%23rev_slider_3_1_wrapper%20.hades%20.tp-bullet%3Ahover%2C%0A%23rev_slider_3_1_wrapper%20.hades%20.tp-bullet.selected%20%7B%0A%09background%3A%23565656%3B%0A%20%20%20%20border-color%3A%23ffffff%3B%0A%7D%0A%0A%23rev_slider_3_1_wrapper%20.hades%20.tp-bullet-image%20%7B%0A%20%20position%3Aabsolute%3B%0A%20%20top%3A-80px%3B%20%0A%20%20left%3A0%3B%0A%20%20width%3A120px%3B%0A%20%20height%3A60px%3B%0A%20%20background-position%3Acenter%20center%3B%0A%20%20background-size%3Acover%3B%0A%20%20visibility%3Ahidden%3B%0A%20%20opacity%3A0%3B%0A%20%20transition%3Aall%200.3s%3B%0A%20%20-webkit-transform-style%3Aflat%3B%0A%20%20transform-style%3Aflat%3B%0A%20%20perspective%3A600%3B%0A%20%20-webkit-perspective%3A600%3B%0A%20%20transform%3A%20rotatex%28-90deg%29%20translatex%28-50%25%29%3B%0A%20%20-webkit-transform%3A%20rotatex%28-90deg%29%20translate%28-50%25%29%3B%0A%20%20box-shadow%3A0px%200px%203px%201px%20rgba%280%2C0%2C0%2C0.2%29%3B%0A%20%20transform-origin%3A50%25%20100%25%3B%0A%20%20-webkit-transform-origin%3A50%25%20100%25%3B%0A%20%20%0A%20%20%0A%7D%0A%23rev_slider_3_1_wrapper%20.hades%20.tp-bullet%3Ahover%20.tp-bullet-image%20%7B%0A%20%20display%3Ablock%3B%0A%20%20opacity%3A1%3B%0A%20%20transform%3A%20rotatex%280deg%29%20translatex%28-50%25%29%3B%0A%20%20-webkit-transform%3A%20rotatex%280deg%29%20translatex%28-50%25%29%3B%0A%20%20visibility%3Avisible%3B%0A%20%20%20%20%7D%0A%23rev_slider_3_1_wrapper%20.hades%20.tp-bullet-title%20%7B%0A%7D%0A%0A"
                        );
                        var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                        if (htmlDiv) {
                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                        } else {
                            var htmlDiv = document.createElement('div');
                            htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                            document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                        }
                    </script>
                    <script>
                        var htmlDivCss = unescape("%0A%0A%0A%0A%0A%0A");
                        var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                        if (htmlDiv) {
                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                        } else {
                            var htmlDiv = document.createElement('div');
                            htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                            document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                        }
                    </script>
                </rs-module-wrap>
                <!-- END REVOLUTION SLIDER -->
            </div>
            <div id="content" class="no-sidebar-page">
                <div id="content-wrap" class="page-builder">
                    <section id="page-content">

<!--                            <div id="pricing" class="tatsu-g89jw47fibce10v0 tatsu-section    tatsu-clearfix" data-title="Pricing" data-headerscheme="background--dark">
                                <div class='tatsu-section-pad clearfix' data-padding='{"d":"90px 0% 90px 0%"}' data-padding-top='90px'>
                                    <div class="tatsu-row-wrap  tatsu-wrap tatsu-row-one-col tatsu-row-has-one-cols tatsu-medium-gutter tatsu-reg-cols  tatsu-clearfix tatsu-g89jw47fn97ezj6k">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-col tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw47frt37vc6" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="special-heading-wrap style3  tatsu-g89jw47fvj9j4fdu oshine-module 1   ">
                                                                <div class="special-heading align-center">
                                                                    <h3 class="special-h-tag">PRICING</h3>
                                                                </div>
                                                                <div class="caption-wrap bottom-caption">
                                                                    <h6 class="caption special-subtitle">The Most
                                                                        Competitive in the market<span class="caption-inner"></span></h6>
                                                                </div>
                                                                <style>
                                                                    .tatsu-g89jw47fvj9j4fdu .top-caption h6 {
                                                                        color: #999999;
                                                                        font-size: 16px;
                                                                    }

                                                                    .tatsu-g89jw47fvj9j4fdu .bottom-caption h6 {
                                                                        color: #999999;
                                                                        font-size: 20px;
                                                                    }

                                                                    .tatsu-g89jw47fvj9j4fdu.special-heading-wrap.style3 .top-caption .caption .caption-inner {
                                                                        background: #efefef;
                                                                    }

                                                                    .tatsu-g89jw47fvj9j4fdu.special-heading-wrap.style3 .bottom-caption .caption .caption-inner {
                                                                        background: #cccccc;
                                                                    }
                                                                </style>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw47frt37vc6.tatsu-column {
                                                        width: 100%;
                                                    }

                                                    .tatsu-g89jw47frt37vc6.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw47frt37vc6>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47frt37vc6>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47frt37vc6>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47frt37vc6>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tatsu-row-wrap  tatsu-wrap tatsu-row-has-four-cols tatsu-no-gutter tatsu-eq-cols  tatsu-clearfix tatsu-g89jw47fww8iotu7">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-fourth tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw47g1h9jcnx6" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <ul class="pricing-table sec-border highlight-no oshine-module be-animate tatsu-g89jw47g4zdrpibn" data-animation="none">
                                                                <li class="pricing-title">
                                                                    <h6 class="sec-color">BASIC PACK</h6>
                                                                </li>
                                                                <li class="pricing-price">
                                                                    <h2 class="price"><span class="currency">$</span>29.99</h2><span class="pricing-duration special-subtitle">1
                                                                        Months</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47hpj6d8vza">
                                                                    <span class="pricing-feature-container">Clean
                                                                        Design</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47hzn11vihw">
                                                                    <span class="pricing-feature-container">Visual Page
                                                                        Builder</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47iewb49gz5">
                                                                    <span class="pricing-feature-container">50+
                                                                        Shortcode Modules</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47ivhflq7io">
                                                                    <span class="pricing-feature-container">Stunning
                                                                        Portfolio Styles</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47j9n5ywfck">
                                                                    <span class="pricing-feature-container">Image and
                                                                        Video Parallax Sections</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47jjpacm0xu">
                                                                    <span class="pricing-feature-container">One Click
                                                                        Import</span>
                                                                </li>
                                                                <li class="pricing-button">
                                                                    <div class="tatsu-module tatsu-normal-button tatsu-button-wrap align-block block-  tatsu-ggirfgx4z016gozx   ">
                                                                        <a class="tatsu-shortcode mediumbtn tatsu-button left-icon    bg-animation-none  " href="https://themeforest.net" style="" data-animation="fadeIn" aria-label="BUY NOW" data-gdpr-atts={}>BUY NOW</a>
                                                                        <style>
                                                                            .tatsu-ggirfgx4z016gozx .tatsu-button:hover {
                                                                                background-color: #38d2d9;
                                                                                color: #ffffff;
                                                                                border-color: #38d2d9;
                                                                            }

                                                                            .tatsu-ggirfgx4z016gozx .tatsu-button {
                                                                                color: #000000;
                                                                                border-width: 1px;
                                                                                border-color: #000000;
                                                                            }
                                                                        </style>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw47g1h9jcnx6.tatsu-column {
                                                        width: 25%;
                                                    }

                                                    .tatsu-g89jw47g1h9jcnx6.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw47g1h9jcnx6>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47g1h9jcnx6>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47g1h9jcnx6>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47g1h9jcnx6>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw47g1h9jcnx6.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-fourth tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw47jxrdrr5cr" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <ul class="pricing-table sec-border highlight-no oshine-module be-animate tatsu-g89jw47k0x5yqfnw" data-animation="none">
                                                                <li class="pricing-title">
                                                                    <h6 class="sec-color">SILVER PACK</h6>
                                                                </li>
                                                                <li class="pricing-price">
                                                                    <h2 class="price"><span class="currency">$</span>69.99</h2><span class="pricing-duration special-subtitle">6
                                                                        Months</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47lig87t84c">
                                                                    <span class="pricing-feature-container">Clean
                                                                        Design</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47lxrcjt9dw">
                                                                    <span class="pricing-feature-container">Visual Page
                                                                        Builder</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47mb85gpv53">
                                                                    <span class="pricing-feature-container">50+
                                                                        Shortcode Modules</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47mr3dj3qqn">
                                                                    <span class="pricing-feature-container">Stunning
                                                                        Portfolio Styles</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47n5c9udt30">
                                                                    <span class="pricing-feature-container">Image and
                                                                        Video Parallax Sections</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47ne91jscnv">
                                                                    <span class="pricing-feature-container">One Click
                                                                        Import</span>
                                                                </li>
                                                                <li class="pricing-button">
                                                                    <div class="tatsu-module tatsu-normal-button tatsu-button-wrap align-block block-  tatsu-ggirfgx5nn93t8qi   ">
                                                                        <a class="tatsu-shortcode mediumbtn tatsu-button left-icon    bg-animation-none  " href="https://themeforest.net" style="" data-animation="fadeIn" aria-label="BUY NOW" data-gdpr-atts={}>BUY NOW</a>
                                                                        <style>
                                                                            .tatsu-ggirfgx5nn93t8qi .tatsu-button:hover {
                                                                                background-color: #38d2d9;
                                                                                color: #ffffff;
                                                                                border-color: #38d2d9;
                                                                            }

                                                                            .tatsu-ggirfgx5nn93t8qi .tatsu-button {
                                                                                color: #000000;
                                                                                border-width: 1px;
                                                                                border-color: #000000;
                                                                            }
                                                                        </style>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw47jxrdrr5cr.tatsu-column {
                                                        width: 25%;
                                                    }

                                                    .tatsu-g89jw47jxrdrr5cr.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw47jxrdrr5cr>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47jxrdrr5cr>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47jxrdrr5cr>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47jxrdrr5cr>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw47jxrdrr5cr.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-fourth tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw47noocn9z3y" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <ul class="pricing-table sec-border highlight-yes oshine-module be-animate tatsu-g89jw47nrcak9rms" data-animation="none">
                                                                <li class="pricing-title">
                                                                    <h6 class="pricing-title-head-tag">
                                                                        GOLD PACK</h6>
                                                                </li>
                                                                <li class="pricing-price">
                                                                    <h2 class="price"><span class="currency">$</span>199.99</h2><span class="pricing-duration special-subtitle">1
                                                                        Year</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47ot3brs786">
                                                                    <span class="pricing-feature-container">Clean
                                                                        Design</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47p2tf8r99m">
                                                                    <span class="pricing-feature-container">Visual Page
                                                                        Builder</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47pbd1zl661">
                                                                    <span class="pricing-feature-container">50+
                                                                        Shortcode Modules</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47pjmaco0ge">
                                                                    <span class="pricing-feature-container">Stunning
                                                                        Portfolio Styles</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47ps2as17i9">
                                                                    <span class="pricing-feature-container">Image and
                                                                        Video Parallax Sections</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47q2h285kqe">
                                                                    <span class="pricing-feature-container">One Click
                                                                        Import</span>
                                                                </li>
                                                                <li class="pricing-button">
                                                                    <div class="tatsu-module tatsu-normal-button tatsu-button-wrap align-block block-  tatsu-ggirfgx6di5xnxil   ">
                                                                        <a class="tatsu-shortcode mediumbtn tatsu-button left-icon    bg-animation-none  " href="https://themeforest.net" style="" data-animation="fadeIn" aria-label="BUY NOW" data-gdpr-atts={}>BUY NOW</a>
                                                                        <style>
                                                                            .tatsu-ggirfgx6di5xnxil .tatsu-button {
                                                                                background-color: #38d2d9;
                                                                                color: #ffffff;
                                                                                border-width: 1px;
                                                                            }

                                                                            .tatsu-ggirfgx6di5xnxil .tatsu-button:hover {
                                                                                background-color: #000000;
                                                                                color: #ffffff;
                                                                            }
                                                                        </style>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw47noocn9z3y.tatsu-column {
                                                        width: 25%;
                                                    }

                                                    .tatsu-g89jw47noocn9z3y.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw47noocn9z3y>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47noocn9z3y>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47noocn9z3y>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47noocn9z3y>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw47noocn9z3y.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-fourth tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw47qdi5mklz1" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <ul class="pricing-table sec-border highlight-no oshine-module be-animate tatsu-g89jw47qg6fg8ppm" data-animation="none">
                                                                <li class="pricing-title">
                                                                    <h6 class="sec-color">BRONZE PACK</h6>
                                                                </li>
                                                                <li class="pricing-price">
                                                                    <h2 class="price"><span class="currency">$</span>49.99</h2><span class="pricing-duration special-subtitle">3
                                                                        Months</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47rim5kbanm">
                                                                    <span class="pricing-feature-container">Clean
                                                                        Design</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47rum4l2aep">
                                                                    <span class="pricing-feature-container">Visual Page
                                                                        Builder</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47s75a759g3">
                                                                    <span class="pricing-feature-container">50+
                                                                        Shortcode Modules</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47skif2f51u">
                                                                    <span class="pricing-feature-container">Stunning
                                                                        Portfolio Styles</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47svl5qwk46">
                                                                    <span class="pricing-feature-container">Image and
                                                                        Video Parallax Sections</span>
                                                                </li>
                                                                <li class="pricing-feature no-highlight tatsu-g89jw47t5o4jmd27">
                                                                    <span class="pricing-feature-container">One Click
                                                                        Import</span>
                                                                </li>
                                                                <li class="pricing-button">
                                                                    <div class="tatsu-module tatsu-normal-button tatsu-button-wrap align-block block-  tatsu-ggirfgx71qa6tsue   ">
                                                                        <a class="tatsu-shortcode mediumbtn tatsu-button left-icon    bg-animation-none  " href="https://themeforest.net" style="" data-animation="fadeIn" aria-label="BUY NOW" data-gdpr-atts={}>BUY NOW</a>
                                                                        <style>
                                                                            .tatsu-ggirfgx71qa6tsue .tatsu-button:hover {
                                                                                background-color: #38d2d9;
                                                                                color: #ffffff;
                                                                                border-color: #38d2d9;
                                                                            }

                                                                            .tatsu-ggirfgx71qa6tsue .tatsu-button {
                                                                                color: #000000;
                                                                                border-width: 1px;
                                                                                border-color: #000000;
                                                                            }
                                                                        </style>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw47qdi5mklz1.tatsu-column {
                                                        width: 25%;
                                                    }

                                                    .tatsu-g89jw47qdi5mklz1.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw47qdi5mklz1>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47qdi5mklz1>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47qdi5mklz1>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47qdi5mklz1>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw47qdi5mklz1.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tatsu-section-background-wrap">
                                    <div class="tatsu-section-background"></div>
                                </div>
                                <style>
                                    .tatsu-g89jw47fibce10v0 {
                                        border-width: 0px 0px px 0px;
                                        border-style: solid;
                                    }

                                    .tatsu-g89jw47fibce10v0 .tatsu-section-pad {
                                        padding: 90px 0% 90px 0%;
                                    }

                                    .tatsu-g89jw47fibce10v0>.tatsu-bottom-divider {
                                        z-index: 9999;
                                    }

                                    .tatsu-g89jw47fibce10v0>.tatsu-top-divider {
                                        z-index: 9999;
                                    }
                                </style>
                            </div>
-->
                        <div class="tatsu-g89jw47tihalj1w7 tatsu-section  tatsu-bg-overlay   tatsu-clearfix" data-title="" data-headerscheme="background--light" style="height: 350px">
                                <div class='tatsu-section-pad clearfix' data-padding='{"d":"90px 0% 90px 0%"}' data-padding-top='90px'>
                                    <div class="tatsu-row-wrap  tatsu-wrap tatsu-row-one-col tatsu-row-has-one-cols tatsu-medium-gutter tatsu-reg-cols  tatsu-clearfix tatsu-g89jw47tm792k5us">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-col tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw47tqpeex0su" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="oshine-module  content-slide-wrap tatsu-g89jw47ttt237tcb  ">
                                                                <div class=" content-slider clearfix">
                                                                    <ul class="clearfix slides content_slider_module clearfix" data-slide-show="1" data-slide-show-speed="4000" data-slide-animation-type="slide">
                                                                        <li class="content_slide slide clearfix">
                                                                            <div class="content_slide_inner" style="width: 60%">
                                                                                <div class="content-slide-content">


                                                                                    <h6 style="text-align: center;">
                                                                                        <h4 style="color: #ffffff;text-align: center;">Grow Your Social Presence</h4>
                                                                                    </h6>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="content_slide slide clearfix">
                                                                            <div class="content_slide_inner" style="width: 60%">
                                                                                <div class="content-slide-content">


                                                                                    <h6 style="text-align: center;">
                                                                                        <h4 style="color: #ffffff;text-align: center;">Boost Your Rankings</h4>
                                                                                    </h6>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li class="content_slide slide clearfix">
                                                                            <div class="content_slide_inner" style="width: 60%">
                                                                                <div class="content-slide-content">


                                                                                    <h6 style="text-align: center;">
                                                                                        <h4 style="color: #ffffff;text-align: center;">Maximize Your Online Visibility</h4>
                                                                                    </h6>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw47tqpeex0su.tatsu-column {
                                                        width: 100%;
                                                    }

                                                    .tatsu-g89jw47tqpeex0su.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw47tqpeex0su>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47tqpeex0su>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47tqpeex0su>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47tqpeex0su>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tatsu-section-background-wrap">
                                    <div class="tatsu-section-background tatsu-bg-vertical-animation"></div>
                                </div>
                                <div class="tatsu-overlay tatsu-section-overlay"></div>
                                <style>
                                    .tatsu-g89jw47tihalj1w7.tatsu-section {
                                        background-color: #f2f3f8;
                                    }

                                    .tatsu-g89jw47tihalj1w7 .tatsu-section-background {
                                        background-image: url(image/Website.jpg);
                                        background-repeat: repeat;
                                        background-attachment: scroll;
                                        background-position: top left;
                                        background-size: cover;
                                    }

                                    .tatsu-g89jw47tihalj1w7 .tatsu-bg-blur {
                                        background-repeat: repeat;
                                        background-attachment: scroll;
                                        background-position: top left;
                                        background-size: cover;
                                    }

                                    .tatsu-g89jw47tihalj1w7 {
                                        border-width: 0px 0px px 0px;
                                        border-style: solid;
                                    }

                                    .tatsu-g89jw47tihalj1w7 .tatsu-section-pad {
                                        padding: 90px 0% 90px 0%;
                                    }

                                    .tatsu-g89jw47tihalj1w7>.tatsu-bottom-divider {
                                        z-index: 9999;
                                    }

                                    .tatsu-g89jw47tihalj1w7>.tatsu-top-divider {
                                        z-index: 9999;
                                    }

                                    .tatsu-g89jw47tihalj1w7 .tatsu-section-overlay {
                                        background: rgba(0, 0, 0, 0.9);
                                        mix-blend-mode: normal;
                                    }
                                </style>
                            </div>
<!--                            <div id="clients" class="tatsu-g89jw47u39n85c3 tatsu-section    tatsu-clearfix" data-title="Clients" data-headerscheme="background--dark">
                                <div class='tatsu-section-pad clearfix' data-padding='{"d":"90px 0% 90px 0%"}' data-padding-top='90px'>
                                    <div class="tatsu-row-wrap  tatsu-wrap tatsu-row-one-col tatsu-row-has-one-cols tatsu-medium-gutter tatsu-reg-cols  tatsu-clearfix tatsu-g89jw47u5y4y5ipk">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-col tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw47u8xeowwgh" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="special-heading-wrap style3  tatsu-g89jw47ub23uyhjo oshine-module 1   ">
                                                                <div class="special-heading align-center">
                                                                    <h3 class="special-h-tag">OUR CLIENTS</h3>
                                                                </div>
                                                                <div class="caption-wrap bottom-caption">
                                                                    <h6 class="caption special-subtitle">Some of the top
                                                                        brands across the globe<span class="caption-inner"></span></h6>
                                                                </div>
                                                                <style>
                                                                    .tatsu-g89jw47ub23uyhjo .top-caption h6 {
                                                                        color: #999999;
                                                                        font-size: 16px;
                                                                    }

                                                                    .tatsu-g89jw47ub23uyhjo .bottom-caption h6 {
                                                                        color: #999999;
                                                                        font-size: 20px;
                                                                    }

                                                                    .tatsu-g89jw47ub23uyhjo.special-heading-wrap.style3 .top-caption .caption .caption-inner {
                                                                        background: #efefef;
                                                                    }

                                                                    .tatsu-g89jw47ub23uyhjo.special-heading-wrap.style3 .bottom-caption .caption .caption-inner {
                                                                        background: #cccccc;
                                                                    }
                                                                </style>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw47u8xeowwgh.tatsu-column {
                                                        width: 100%;
                                                    }

                                                    .tatsu-g89jw47u8xeowwgh.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw47u8xeowwgh>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47u8xeowwgh>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47u8xeowwgh>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47u8xeowwgh>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tatsu-row-wrap  tatsu-wrap tatsu-row-has-four-cols tatsu-zero-margin tatsu-no-gutter tatsu-eq-cols  tatsu-clearfix tatsu-g89jw47uby7mahit">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-fourth tatsu-column-align-middle tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw47uf29211ky" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw47ugp438now  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw47ugp438now.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <p><img class="alignnone size-full wp-image-2713" src="image/oshin-main-flow.jpg" alt="oshin-main-flow" width="800" height="600" /></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw47uf29211ky.tatsu-column {
                                                        width: 25%;
                                                    }

                                                    .tatsu-g89jw47uf29211ky.tatsu-column>.tatsu-column-inner>.tatsu-column-pad-wrap>.tatsu-column-pad {
                                                        padding: 5px 5px 5px 5px;
                                                    }

                                                    .tatsu-g89jw47uf29211ky.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw47uf29211ky>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47uf29211ky>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47uf29211ky>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47uf29211ky>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw47uf29211ky.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-fourth tatsu-column-align-middle tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw47uj68ycujm" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw47uku8ri83r  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw47uku8ri83r.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <p><img class="alignnone size-full wp-image-2712" src="image/Marks_2048x1536_144dpi_12_1000.jpg" alt="Marks_2048x1536_144dpi_12_1000" width="800" height="600" /></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw47uj68ycujm.tatsu-column {
                                                        width: 25%;
                                                    }

                                                    .tatsu-g89jw47uj68ycujm.tatsu-column>.tatsu-column-inner>.tatsu-column-pad-wrap>.tatsu-column-pad {
                                                        padding: 5px 5px 5px 5px;
                                                    }

                                                    .tatsu-g89jw47uj68ycujm.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw47uj68ycujm>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47uj68ycujm>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47uj68ycujm>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47uj68ycujm>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw47uj68ycujm.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-fourth tatsu-column-align-middle tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw47un22syvhh" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw47uom25z6zi  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw47uom25z6zi.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <p><img class="alignnone size-full wp-image-2711" src="image/Marks_2048x1536_144dpi_11_1000.jpg" alt="Marks_2048x1536_144dpi_11_1000" width="800" height="600" /></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw47un22syvhh.tatsu-column {
                                                        width: 25%;
                                                    }

                                                    .tatsu-g89jw47un22syvhh.tatsu-column>.tatsu-column-inner>.tatsu-column-pad-wrap>.tatsu-column-pad {
                                                        padding: 5px 5px 5px 5px;
                                                    }

                                                    .tatsu-g89jw47un22syvhh.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw47un22syvhh>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47un22syvhh>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47un22syvhh>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47un22syvhh>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw47un22syvhh.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-fourth tatsu-column-align-middle tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw47ur01npsds" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw47ut45upuk9  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw47ut45upuk9.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <p><img class="alignnone size-full wp-image-2710" src="image/Marks_2048x1536_144dpi_9_1000.jpg" alt="Marks_2048x1536_144dpi_9_1000" width="800" height="600" /></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw47ur01npsds.tatsu-column {
                                                        width: 25%;
                                                    }

                                                    .tatsu-g89jw47ur01npsds.tatsu-column>.tatsu-column-inner>.tatsu-column-pad-wrap>.tatsu-column-pad {
                                                        padding: 5px 5px 5px 5px;
                                                    }

                                                    .tatsu-g89jw47ur01npsds.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw47ur01npsds>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47ur01npsds>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47ur01npsds>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47ur01npsds>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw47ur01npsds.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tatsu-row-wrap  tatsu-wrap tatsu-row-has-four-cols tatsu-zero-margin tatsu-no-gutter tatsu-eq-cols  tatsu-clearfix tatsu-g89jw47uuicnj22p">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-fourth tatsu-column-align-middle tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw47uxc2bgepd" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw47uywefkybn  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw47uywefkybn.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <p><img class="alignnone size-full wp-image-2709" src="image/Marks_2048x1536_144dpi_8_1000.jpg" alt="Marks_2048x1536_144dpi_8_1000" width="800" height="600" /></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw47uxc2bgepd.tatsu-column {
                                                        width: 25%;
                                                    }

                                                    .tatsu-g89jw47uxc2bgepd.tatsu-column>.tatsu-column-inner>.tatsu-column-pad-wrap>.tatsu-column-pad {
                                                        padding: 5px 5px 5px 5px;
                                                    }

                                                    .tatsu-g89jw47uxc2bgepd.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw47uxc2bgepd>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47uxc2bgepd>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47uxc2bgepd>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47uxc2bgepd>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw47uxc2bgepd.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-fourth tatsu-column-align-middle tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw47v1450rts0" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw47v2o5kogfk  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw47v2o5kogfk.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <p><img class="alignnone size-full wp-image-2708" src="image/Marks_2048x1536_144dpi_7_1000.jpg" alt="Marks_2048x1536_144dpi_7_1000" width="800" height="600" /></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw47v1450rts0.tatsu-column {
                                                        width: 25%;
                                                    }

                                                    .tatsu-g89jw47v1450rts0.tatsu-column>.tatsu-column-inner>.tatsu-column-pad-wrap>.tatsu-column-pad {
                                                        padding: 5px 5px 5px 5px;
                                                    }

                                                    .tatsu-g89jw47v1450rts0.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw47v1450rts0>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47v1450rts0>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47v1450rts0>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47v1450rts0>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw47v1450rts0.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-fourth tatsu-column-align-middle tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw47v4t3fiihp" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw47v6jg2cqxs  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw47v6jg2cqxs.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <p><img class="alignnone size-full wp-image-2707" src="image/Marks_2048x1536_144dpi_6_1000.jpg" alt="Marks_2048x1536_144dpi_6_1000" width="800" height="600" /></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw47v4t3fiihp.tatsu-column {
                                                        width: 25%;
                                                    }

                                                    .tatsu-g89jw47v4t3fiihp.tatsu-column>.tatsu-column-inner>.tatsu-column-pad-wrap>.tatsu-column-pad {
                                                        padding: 5px 5px 5px 5px;
                                                    }

                                                    .tatsu-g89jw47v4t3fiihp.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw47v4t3fiihp>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47v4t3fiihp>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47v4t3fiihp>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47v4t3fiihp>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw47v4t3fiihp.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-fourth tatsu-column-align-middle tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw47v8q3hpegl" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw47vat3q3zx0  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw47vat3q3zx0.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <p><img class="alignnone size-full wp-image-2703" src="image/Marks_2048x1536_144dpi_2_1000.jpg" alt="Marks_2048x1536_144dpi_2_1000" width="800" height="600" /></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw47v8q3hpegl.tatsu-column {
                                                        width: 25%;
                                                    }

                                                    .tatsu-g89jw47v8q3hpegl.tatsu-column>.tatsu-column-inner>.tatsu-column-pad-wrap>.tatsu-column-pad {
                                                        padding: 5px 5px 5px 5px;
                                                    }

                                                    .tatsu-g89jw47v8q3hpegl.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw47v8q3hpegl>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47v8q3hpegl>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47v8q3hpegl>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47v8q3hpegl>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw47v8q3hpegl.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tatsu-section-background-wrap">
                                    <div class="tatsu-section-background"></div>
                                </div>
                                <style>
                                    .tatsu-g89jw47u39n85c3 {
                                        border-width: 0px 0px px 0px;
                                        border-style: solid;
                                    }

                                    .tatsu-g89jw47u39n85c3 .tatsu-section-pad {
                                        padding: 90px 0% 90px 0%;
                                    }

                                    .tatsu-g89jw47u39n85c3>.tatsu-bottom-divider {
                                        z-index: 9999;
                                    }

                                    .tatsu-g89jw47u39n85c3>.tatsu-top-divider {
                                        z-index: 9999;
                                    }
                                </style>
                            </div>
-->
                        <div class="clearfix">
                            <div id="about" class="tatsu-g89jw45nch4akvs3 tatsu-section    tatsu-clearfix" data-title="About" data-headerscheme="background--dark">
                                <div class='tatsu-section-pad clearfix' data-padding='{"d":"90px 0% 90px 0%"}' data-padding-top='90px'>
                                    <div class="tatsu-row-wrap  tatsu-wrap tatsu-row-one-col tatsu-row-has-one-cols tatsu-medium-gutter tatsu-reg-cols  tatsu-clearfix tatsu-g89jw45nfn9a9r57">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-col tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45niu8qwz2l" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-normal-icon tatsu-icon-shortcode align-center tatsu-g89jw45nl6e0h1da   ">
                                                                <style>
                                                                    .tatsu-g89jw45nl6e0h1da .tatsu-icon {
                                                                        color: #000000;
                                                                    }

                                                                    .tatsu-g89jw45nl6e0h1da .tatsu-icon:hover {
                                                                        color: #000000;
                                                                    }

                                                                    .tatsu-g89jw45nl6e0h1da {
                                                                        border-width: 0px 0px 0px 0px;
                                                                    }
                                                                </style>
                                                                <a href="#" class="tatsu-icon-wrap plain     " aria-label="icon-anchor" data-gdpr-atts={}>
                                                                    <img class="sticky-logo" src="image/roots_home.png" style="width: 25%;" alt="Roots">
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45niu8qwz2l.tatsu-column {
                                                        width: 100%;
                                                    }

                                                    .tatsu-g89jw45niu8qwz2l.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45niu8qwz2l>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45niu8qwz2l>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45niu8qwz2l>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45niu8qwz2l>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tatsu-row-wrap  tatsu-wrap tatsu-row-one-col tatsu-row-has-one-cols tatsu-medium-gutter tatsu-reg-cols  tatsu-clearfix tatsu-g89jw45nm66kf1bt">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-col tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45noxetupj0" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw45nqpdv4u46  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw45nqpdv4u46.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            width: 75%;
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <p style="text-align: center;"><span style="font-size: 18pt; line-height: 1.8; color: #000000;">To help brands penetrate new markets, empower existing markets, and compete at the highest level, we offer full integrated creative solutions set. We treat visual communications as one of the most effective and engaging techniques, we oversee all aspects that help customers to understand the brand better.</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45noxetupj0.tatsu-column {
                                                        width: 100%;
                                                    }

                                                    .tatsu-g89jw45noxetupj0.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45noxetupj0>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45noxetupj0>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45noxetupj0>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45noxetupj0>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tatsu-row-wrap  tatsu-wrap tatsu-row-one-col tatsu-row-has-one-cols tatsu-medium-gutter tatsu-reg-cols  tatsu-clearfix tatsu-g89jw45ol5ew2qj">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-col tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45oqi1rwg98" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-normal-button tatsu-button-wrap align-block block-center  tatsu-g89jw45ovl8v6o0l   ">
                                                                <a class="tatsu-shortcode mediumbtn tatsu-button right-icon    bg-animation-slide-top  " href="#portfolio" style="background-image: linear-gradient(to bottom, transparent 50%, #38d2d9 50%);" aria-label="OUR WORK" data-gdpr-atts={}>OUR WORK<i class="tatsu-icon icon-arrow-down2"></i></a>
                                                                <style>
                                                                    .tatsu-g89jw45ovl8v6o0l .tatsu-button {
                                                                        color: #aaaaaa;
                                                                        border-width: 2px;
                                                                        border-color: #aaaaaa;
                                                                    }

                                                                    .tatsu-g89jw45ovl8v6o0l .tatsu-button:hover {
                                                                        color: #ffffff;
                                                                        border-color: #38d2d9;
                                                                    }
                                                                </style>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45oqi1rwg98.tatsu-column {
                                                        width: 100%;
                                                    }

                                                    .tatsu-g89jw45oqi1rwg98.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45oqi1rwg98>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45oqi1rwg98>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45oqi1rwg98>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45oqi1rwg98>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tatsu-section-background-wrap">
                                    <div class="tatsu-section-background"></div>
                                </div>
                                <style>
                                    .tatsu-g89jw45nch4akvs3 {
                                        border-width: 0px 0px px 0px;
                                        border-style: solid;
                                    }

                                    .tatsu-g89jw45nch4akvs3 .tatsu-section-pad {
                                        padding: 90px 0% 90px 0%;
                                    }

                                    .tatsu-g89jw45nch4akvs3>.tatsu-bottom-divider {
                                        z-index: 9999;
                                    }

                                    .tatsu-g89jw45nch4akvs3>.tatsu-top-divider {
                                        z-index: 9999;
                                    }
                                </style>
                            </div>
                            <div id="team" class="tatsu-g89jw45oz78udzjb tatsu-section    tatsu-clearfix" data-title="Team" data-headerscheme="background--dark">
                                <div class='tatsu-section-pad clearfix' data-padding='{"d":"90px 0% 90px 0%"}' data-padding-top='90px'>
                                    <div class="tatsu-row-wrap  tatsu-wrap tatsu-row-one-col tatsu-row-has-one-cols tatsu-medium-gutter tatsu-reg-cols  tatsu-clearfix tatsu-g89jw45p3h6rfeqv">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-col tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45p7s85t4ev" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="special-heading-wrap style3  tatsu-g89jw45pb92y93g9 oshine-module 1   ">
                                                                <div class="special-heading align-center">
                                                                    <h3 class="special-h-tag">OUR TEAM</h3>
                                                                </div>
                                                                <div class="caption-wrap bottom-caption">
                                                                    <h6 class="caption special-subtitle">We put our
                                                                        people first<span class="caption-inner"></span>
                                                                    </h6>
                                                                </div>
                                                                <style>
                                                                    .tatsu-g89jw45pb92y93g9 .top-caption h6 {
                                                                        color: #999999;
                                                                        font-size: 16px;
                                                                    }

                                                                    .tatsu-g89jw45pb92y93g9 .bottom-caption h6 {
                                                                        color: #999999;
                                                                        font-size: 20px;
                                                                    }

                                                                    .tatsu-g89jw45pb92y93g9.special-heading-wrap.style3 .top-caption .caption .caption-inner {
                                                                        background: #efefef;
                                                                    }

                                                                    .tatsu-g89jw45pb92y93g9.special-heading-wrap.style3 .bottom-caption .caption .caption-inner {
                                                                        background: #cccccc;
                                                                    }
                                                                </style>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45p7s85t4ev.tatsu-column {
                                                        width: 100%;
                                                    }

                                                    .tatsu-g89jw45p7s85t4ev.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45p7s85t4ev>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45p7s85t4ev>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45p7s85t4ev>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45p7s85t4ev>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tatsu-row-wrap  tatsu-wrap tatsu-row-has-four-cols tatsu-medium-gutter tatsu-reg-cols  tatsu-clearfix tatsu-g89jw45pciaqdh3a">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-fourth tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45pgr8moose" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-g89jw45pla6j91lv team-shortcode-wrap oshine-module">
                                                                <div class="element  bw_to_c style5-title">
                                                                    <div class="element-inner">
                                                                        <div class="flip-wrap">

                                                                        </div>
                                                                        <div class="thumb-overlay">
                                                                            <div class="thumb-bg">
                                                                                <div class="display-table">
                                                                                    <div class="display-table-cell vertical-align-middle">
                                                                                        <div class="team-wrap clearfix">
                                                                                            <h6 class="team-title">
                                                                                                Mohamed Sayed</h6>
                                                                                            <p class="designation">
                                                                                               Digital Marketing & Video production
                                                                                            </p>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <style>
                                                                    .tatsu-g89jw45pla6j91lv .team-title {
                                                                        color: #222222;
                                                                    }

                                                                    .tatsu-g89jw45pla6j91lv .designation {
                                                                        color: #222222;
                                                                    }

                                                                    .tatsu-g89jw45pla6j91lv i {
                                                                        color: #ffffff;
                                                                    }

                                                                    .tatsu-g89jw45pla6j91lv i:hover {
                                                                        color: #ffffff;
                                                                    }

                                                                    .tatsu-g89jw45pla6j91lv .team-wrap {
                                                                        text-align: center;
                                                                    }

                                                                    .tatsu-g89jw45pla6j91lv .team-social {
                                                                        background: rgba(0, 0, 0, 1);
                                                                    }
                                                                </style>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45pgr8moose.tatsu-column {
                                                        width: 25%;
                                                    }

                                                    .tatsu-g89jw45pgr8moose.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45pgr8moose>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45pgr8moose>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45pgr8moose>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45pgr8moose>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw45pgr8moose.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-fourth tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45poa178ozj" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-g89jw45pse8llyvu team-shortcode-wrap oshine-module">
                                                                <div class="element  bw_to_c style5-title">
                                                                    <div class="element-inner">
                                                                        <div class="flip-wrap">
                                                                            <!--<div class="flip-img-wrap zoom-in-effect">
                                                                                <img src="image/team-v8-3.jpg" alt="Lisa" />
                                                                                <ul class="team-social clearfix over" style="background: rgba(0,0,0,1)">
                                                                                    <li class="icon-shortcode"><a href="www.twitter.com" class="font-icon tatsu-icon team_icons" target="_blank"><i class="icon-twitter"></i></a>
                                                                                    </li>
                                                                                    <li class="icon-shortcode"><a href="www.google.com" class="font-icon tatsu-icon team_icons" target="_blank"><i class="icon-gplus"></i></a>
                                                                                    </li>
                                                                                    <li class="icon-shortcode"><a href="www.linkedin.com" class="font-icon tatsu-icon team_icons" target="_blank"><i class="icon-linkedin"></i></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>-->
                                                                        </div>
                                                                        <div class="thumb-overlay">
                                                                            <div class="thumb-bg">
                                                                                <div class="display-table">
                                                                                    <div class="display-table-cell vertical-align-middle">
                                                                                        <div class="team-wrap clearfix">
                                                                                            <h6 class="team-title">
                                                                                                Esraa Abd ElSamed</h6>
                                                                                            <p class="designation">Social Media & Branding
                                                                                            </p>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <style>
                                                                    .tatsu-g89jw45pse8llyvu .team-title {
                                                                        color: #222222;
                                                                    }

                                                                    .tatsu-g89jw45pse8llyvu .designation {
                                                                        color: #222222;
                                                                    }

                                                                    .tatsu-g89jw45pse8llyvu i {
                                                                        color: #ffffff;
                                                                    }

                                                                    .tatsu-g89jw45pse8llyvu i:hover {
                                                                        color: #ffffff;
                                                                    }

                                                                    .tatsu-g89jw45pse8llyvu .team-wrap {
                                                                        text-align: center;
                                                                    }

                                                                    .tatsu-g89jw45pse8llyvu .team-social {
                                                                        background: rgba(0, 0, 0, 1);
                                                                    }
                                                                </style>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45poa178ozj.tatsu-column {
                                                        width: 25%;
                                                    }

                                                    .tatsu-g89jw45poa178ozj.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45poa178ozj>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45poa178ozj>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45poa178ozj>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45poa178ozj>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw45poa178ozj.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-fourth tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45pu690kxtp" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-g89jw45px1fjx1ss team-shortcode-wrap oshine-module">
                                                                <div class="element  bw_to_c style5-title">
                                                                    <div class="element-inner">
                                                                        <div class="flip-wrap">
                                                                            <!--<div class="flip-img-wrap zoom-in-effect">
                                                                                <img src="image/team-v8-2.jpg" alt="James" />
                                                                                <ul class="team-social clearfix over" style="background: rgba(0,0,0,1)">
                                                                                    <li class="icon-shortcode"><a href="www.twitter.com" class="font-icon tatsu-icon team_icons" target="_blank"><i class="icon-twitter"></i></a>
                                                                                    </li>
                                                                                    <li class="icon-shortcode"><a href="www.google.com" class="font-icon tatsu-icon team_icons" target="_blank"><i class="icon-gplus"></i></a>
                                                                                    </li>
                                                                                    <li class="icon-shortcode"><a href="www.linkedin.com" class="font-icon tatsu-icon team_icons" target="_blank"><i class="icon-linkedin"></i></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>-->
                                                                        </div>
                                                                        <div class="thumb-overlay">
                                                                            <div class="thumb-bg">
                                                                                <div class="display-table">
                                                                                    <div class="display-table-cell vertical-align-middle">
                                                                                        <div class="team-wrap clearfix">
                                                                                            <h6 class="team-title">
                                                                                                Mohamed Atef</h6>
                                                                                            <p class="designation">
                                                                                                Public Relations
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <style>
                                                                    .tatsu-g89jw45px1fjx1ss .team-title {
                                                                        color: #222222;
                                                                    }

                                                                    .tatsu-g89jw45px1fjx1ss .designation {
                                                                        color: #222222;
                                                                    }

                                                                    .tatsu-g89jw45px1fjx1ss i {
                                                                        color: #ffffff;
                                                                    }

                                                                    .tatsu-g89jw45px1fjx1ss i:hover {
                                                                        color: #ffffff;
                                                                    }

                                                                    .tatsu-g89jw45px1fjx1ss .team-wrap {
                                                                        text-align: center;
                                                                    }

                                                                    .tatsu-g89jw45px1fjx1ss .team-social {
                                                                        background: rgba(0, 0, 0, 1);
                                                                    }
                                                                </style>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45pu690kxtp.tatsu-column {
                                                        width: 25%;
                                                    }

                                                    .tatsu-g89jw45pu690kxtp.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45pu690kxtp>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45pu690kxtp>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45pu690kxtp>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45pu690kxtp>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw45pu690kxtp.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-fourth tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45pyr8qq8v9" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-g89jw45q1qeaiawi team-shortcode-wrap oshine-module">
                                                                <div class="element  bw_to_c style5-title">
                                                                    <div class="element-inner">
                                                                        <div class="flip-wrap">
                                                                          <!--  <div class="flip-img-wrap zoom-in-effect">
                                                                                <img src="image/team-v8-4.jpg" alt="Jane" />
                                                                                <ul class="team-social clearfix over" style="background: rgba(0,0,0,1)">
                                                                                    <li class="icon-shortcode"><a href="www.twitter.com" class="font-icon tatsu-icon team_icons" target="_blank"><i class="icon-twitter"></i></a>
                                                                                    </li>
                                                                                    <li class="icon-shortcode"><a href="www.google.com" class="font-icon tatsu-icon team_icons" target="_blank"><i class="icon-gplus"></i></a>
                                                                                    </li>
                                                                                    <li class="icon-shortcode"><a href="www.linkedin.com" class="font-icon tatsu-icon team_icons" target="_blank"><i class="icon-linkedin"></i></a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>-->
                                                                        </div>
                                                                        <div class="thumb-overlay">
                                                                            <div class="thumb-bg">
                                                                                <div class="display-table">
                                                                                    <div class="display-table-cell vertical-align-middle">
                                                                                        <div class="team-wrap clearfix">
                                                                                            <h6 class="team-title">
                                                                                                Omar Tarek</h6>
                                                                                            <p class="designation">Web Design & Development
                                                                                            </p>

                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <style>
                                                                    .tatsu-g89jw45q1qeaiawi .team-title {
                                                                        color: #222222;
                                                                    }

                                                                    .tatsu-g89jw45q1qeaiawi .designation {
                                                                        color: #222222;
                                                                    }

                                                                    .tatsu-g89jw45q1qeaiawi i {
                                                                        color: #ffffff;
                                                                    }

                                                                    .tatsu-g89jw45q1qeaiawi i:hover {
                                                                        color: #ffffff;
                                                                    }

                                                                    .tatsu-g89jw45q1qeaiawi .team-wrap {
                                                                        text-align: center;
                                                                    }

                                                                    .tatsu-g89jw45q1qeaiawi .team-social {
                                                                        background: rgba(0, 0, 0, 1);
                                                                    }
                                                                </style>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45pyr8qq8v9.tatsu-column {
                                                        width: 25%;
                                                    }

                                                    .tatsu-g89jw45pyr8qq8v9.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45pyr8qq8v9>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45pyr8qq8v9>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45pyr8qq8v9>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45pyr8qq8v9>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw45pyr8qq8v9.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tatsu-section-background-wrap">
                                    <div class="tatsu-section-background"></div>
                                </div>
                                <style>
                                    .tatsu-g89jw45oz78udzjb.tatsu-section {
                                        background-color: #f2f3f8;
                                    }

                                    .tatsu-g89jw45oz78udzjb {
                                        border-width: 0px 0px px 0px;
                                        border-style: solid;
                                    }

                                    .tatsu-g89jw45oz78udzjb .tatsu-section-pad {
                                        padding: 90px 0% 90px 0%;
                                    }

                                    .tatsu-g89jw45oz78udzjb>.tatsu-bottom-divider {
                                        z-index: 9999;
                                    }

                                    .tatsu-g89jw45oz78udzjb>.tatsu-top-divider {
                                        z-index: 9999;
                                    }
                                </style>
                            </div>
                            <div id="services" class="tatsu-g89jw45q3i7j4tlm tatsu-section    tatsu-clearfix" data-title="Services" data-headerscheme="background--dark">
                                <div class='tatsu-section-pad clearfix' data-padding='{"d":"90px 0% 90px 0%"}' data-padding-top='90px'>
                                    <div class="tatsu-row-wrap  tatsu-wrap tatsu-row-one-col tatsu-row-has-one-cols tatsu-medium-gutter tatsu-reg-cols  tatsu-clearfix tatsu-g89jw45q5m65smnw">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-col tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45q8b3kf8xc" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="special-heading-wrap style3  tatsu-g89jw45qales36mc oshine-module 1   ">
                                                                <div class="special-heading align-center">
                                                                    <h3 class="special-h-tag">OUR SERVICES</h3>
                                                                </div>
                                                                <div class="caption-wrap bottom-caption">
                                                                    <h6 class="caption special-subtitle">There is magic
                                                                        in what we do<span class="caption-inner"></span>
                                                                    </h6>
                                                                </div>
                                                                <style>
                                                                    .tatsu-g89jw45qales36mc .top-caption h6 {
                                                                        color: #999999;
                                                                        font-size: 16px;
                                                                    }

                                                                    .tatsu-g89jw45qales36mc .bottom-caption h6 {
                                                                        color: #999999;
                                                                        font-size: 20px;
                                                                    }

                                                                    .tatsu-g89jw45qales36mc.special-heading-wrap.style3 .top-caption .caption .caption-inner {
                                                                        background: #efefef;
                                                                    }

                                                                    .tatsu-g89jw45qales36mc.special-heading-wrap.style3 .bottom-caption .caption .caption-inner {
                                                                        background: #cccccc;
                                                                    }
                                                                </style>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45q8b3kf8xc.tatsu-column {
                                                        width: 100%;
                                                    }

                                                    .tatsu-g89jw45q8b3kf8xc.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45q8b3kf8xc>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45q8b3kf8xc>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45q8b3kf8xc>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45q8b3kf8xc>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tatsu-row-wrap  tatsu-wrap tatsu-row-has-three-cols tatsu-medium-gutter tatsu-reg-cols  tatsu-clearfix tatsu-g89jw45qbf8au1m5">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-third tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45qed4ljlf1" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-normal-icon tatsu-icon-shortcode align-center tatsu-g89jw45qgt2bpqkb   ">
                                                                <style>
                                                                    .tatsu-g89jw45qgt2bpqkb .tatsu-icon {
                                                                        background-color: #38d2d9;
                                                                        color: #ffffff;
                                                                        border-color: #000000;
                                                                    }

                                                                    .tatsu-g89jw45qgt2bpqkb .tatsu-icon:hover {
                                                                        background-color: #fafbfd;
                                                                        color: #38d2d9;
                                                                        border-color: #eb4949;
                                                                    }

                                                                    .tatsu-g89jw45qgt2bpqkb {
                                                                        border-width: 0px 0px 0px 0px;
                                                                    }
                                                                </style>
                                                                <a href="#" class="tatsu-icon-wrap circle     " aria-label="icon-strategy" data-gdpr-atts={}><i class="tatsu-icon tatsu-custom-icon tatsu-custom-icon-class icon-strategy large circle" data-animation="none" data-animation-delay="0"></i></a>
                                                            </div>
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw45qhgc765j6  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw45qhgc765j6.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <h6 style="text-align: center;">Branding</h6>
                                                                    <p style="text-align: center;">A creative advertising and digital experienc</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45qed4ljlf1.tatsu-column {
                                                        width: 33.33%;
                                                    }

                                                    .tatsu-g89jw45qed4ljlf1.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45qed4ljlf1>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45qed4ljlf1>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45qed4ljlf1>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45qed4ljlf1>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw45qed4ljlf1.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-third tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45qkp7dw3yw" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-normal-icon tatsu-icon-shortcode align-center tatsu-g89jw45qn4g08ek8   ">
                                                                <style>
                                                                    .tatsu-g89jw45qn4g08ek8 .tatsu-icon {
                                                                        background-color: #38d2d9;
                                                                        color: #ffffff;
                                                                        border-color: #38d2d9;
                                                                    }

                                                                    .tatsu-g89jw45qn4g08ek8 .tatsu-icon:hover {
                                                                        background-color: #fafbfd;
                                                                        color: #38d2d9;
                                                                        border-color: #000000;
                                                                    }

                                                                    .tatsu-g89jw45qn4g08ek8 {
                                                                        border-width: 0px 0px 0px 0px;
                                                                    }
                                                                </style>
                                                                <a href="#" class="tatsu-icon-wrap circle     " aria-label="icon-lightbulb" data-gdpr-atts={}><i class="tatsu-icon tatsu-custom-icon tatsu-custom-icon-class icon-lightbulb large circle" data-animation="none" data-animation-delay="0"></i></a>
                                                            </div>
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw45qnqfiptwo  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw45qnqfiptwo.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <h6 style="text-align: center;">Marketing</h6>
                                                                    <p style="text-align: center;">Blending great ideas and technology, we ensure that you have all the ingredients to make the right noises in the market.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45qkp7dw3yw.tatsu-column {
                                                        width: 33.33%;
                                                    }

                                                    .tatsu-g89jw45qkp7dw3yw.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45qkp7dw3yw>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45qkp7dw3yw>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45qkp7dw3yw>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45qkp7dw3yw>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw45qkp7dw3yw.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-third tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45qqa52pygp" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-normal-icon tatsu-icon-shortcode align-center tatsu-g89jw45qswbhdqtl   ">
                                                                <style>
                                                                    .tatsu-g89jw45qswbhdqtl .tatsu-icon {
                                                                        background-color: #38d2d9;
                                                                        color: #ffffff;
                                                                        border-color: #38d2d9;
                                                                    }

                                                                    .tatsu-g89jw45qswbhdqtl .tatsu-icon:hover {
                                                                        background-color: #fafbfd;
                                                                        color: #38d2d9;
                                                                        border-color: #000000;
                                                                    }

                                                                    .tatsu-g89jw45qswbhdqtl {
                                                                        border-width: 0px 0px 0px 0px;
                                                                    }
                                                                </style>
                                                                <a href="#" class="tatsu-icon-wrap circle     " aria-label="icon-desktop" data-gdpr-atts={}><i class="tatsu-icon tatsu-custom-icon tatsu-custom-icon-class icon-desktop large circle" data-animation="none" data-animation-delay="0"></i></a>
                                                            </div>
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw45qth8roxmf  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw45qth8roxmf.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <h6 style="text-align: center;">Development</h6>
                                                                    <p style="text-align: center;">Web development is a broad term for business participating in the development of websites for the World Wide Web or the Intranet. Web development ranges from developing</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45qqa52pygp.tatsu-column {
                                                        width: 33.33%;
                                                    }

                                                    .tatsu-g89jw45qqa52pygp.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45qqa52pygp>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45qqa52pygp>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45qqa52pygp>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45qqa52pygp>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw45qqa52pygp.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tatsu-row-wrap  tatsu-wrap tatsu-row-has-three-cols tatsu-medium-gutter tatsu-reg-cols  tatsu-clearfix tatsu-g89jw45quw8ut121">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-third tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45qyzasw476" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-normal-icon tatsu-icon-shortcode align-center tatsu-g89jw45r2d250c9d   ">
                                                                <style>
                                                                    .tatsu-g89jw45r2d250c9d .tatsu-icon {
                                                                        background-color: #38d2d9;
                                                                        color: #ffffff;
                                                                        border-color: #000000;
                                                                    }

                                                                    .tatsu-g89jw45r2d250c9d .tatsu-icon:hover {
                                                                        background-color: #fafbfd;
                                                                        color: #38d2d9;
                                                                        border-color: #eb4949;
                                                                    }

                                                                    .tatsu-g89jw45r2d250c9d {
                                                                        border-width: 0px 0px 0px 0px;
                                                                    }
                                                                </style>
                                                                <a href="#" class="tatsu-icon-wrap circle     " aria-label="icon-pencil2" data-gdpr-atts={}><i class="tatsu-icon tatsu-custom-icon tatsu-custom-icon-class icon-pencil2 large circle" data-animation="none" data-animation-delay="0"></i></a>
                                                            </div>
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw45r2zafj6lu  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw45r2zafj6lu.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <h6 style="text-align: center;">Web Design</h6>
                                                                    <p style="text-align: center;">It is the process of planning and implementing multimedia contents across the network, using technology patterns

                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45qyzasw476.tatsu-column {
                                                        width: 33.33%;
                                                    }

                                                    .tatsu-g89jw45qyzasw476.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45qyzasw476>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45qyzasw476>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45qyzasw476>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45qyzasw476>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw45qyzasw476.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-third tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45r5fga4e6c" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-normal-icon tatsu-icon-shortcode align-center tatsu-g89jw45r7wad9qmx   ">
                                                                <style>
                                                                    .tatsu-g89jw45r7wad9qmx .tatsu-icon {
                                                                        background-color: #38d2d9;
                                                                        color: #ffffff;
                                                                        border-color: #000000;
                                                                    }

                                                                    .tatsu-g89jw45r7wad9qmx .tatsu-icon:hover {
                                                                        background-color: #fafbfd;
                                                                        color: #38d2d9;
                                                                        border-color: #eb4949;
                                                                    }

                                                                    .tatsu-g89jw45r7wad9qmx {
                                                                        border-width: 0px 0px 0px 0px;
                                                                    }
                                                                </style>
                                                                <a href="#" class="tatsu-icon-wrap circle     " aria-label="icon-linegraph" data-gdpr-atts={}><i class="tatsu-icon tatsu-custom-icon tatsu-custom-icon-class icon-linegraph large circle" data-animation="none" data-animation-delay="0"></i></a>
                                                            </div>
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw45r8h544tw9  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw45r8h544tw9.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <h6 style="text-align: center;">Social Media</h6>
                                                                    <p style="text-align: center;">Engage and interact with your customers. Drive targeted traffic, increase your reach, build brand loyalty and improve relationships with your audience.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45r5fga4e6c.tatsu-column {
                                                        width: 33.33%;
                                                    }

                                                    .tatsu-g89jw45r5fga4e6c.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45r5fga4e6c>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45r5fga4e6c>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45r5fga4e6c>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45r5fga4e6c>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw45r5fga4e6c.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-third tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45rav3t93v7" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-normal-icon tatsu-icon-shortcode align-center tatsu-g89jw45rdhcar3p5   ">
                                                                <style>
                                                                    .tatsu-g89jw45rdhcar3p5 .tatsu-icon {
                                                                        background-color: #38d2d9;
                                                                        color: #ffffff;
                                                                        border-color: #000000;
                                                                    }

                                                                    .tatsu-g89jw45rdhcar3p5 .tatsu-icon:hover {
                                                                        background-color: #fafbfd;
                                                                        color: #38d2d9;
                                                                        border-color: #eb4949;
                                                                    }

                                                                    .tatsu-g89jw45rdhcar3p5 {
                                                                        border-width: 0px 0px 0px 0px;
                                                                    }
                                                                </style>
                                                                <a href="#" class="tatsu-icon-wrap circle     " aria-label="icon-basket" data-gdpr-atts={}><i class="tatsu-icon tatsu-custom-icon tatsu-custom-icon-class icon-basket large circle" data-animation="none" data-animation-delay="0"></i></a>
                                                            </div>
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw45re3667bup  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw45re3667bup.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <h6 style="text-align: center;">Video Production</h6>
                                                                    <p style="text-align: center;">Delivering high production value for your video production is our mission. We are specialists for ads, social media videos. We have a committed and energetic approach, making the most of each project’s time & budgetary constraints.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45rav3t93v7.tatsu-column {
                                                        width: 33.33%;
                                                    }

                                                    .tatsu-g89jw45rav3t93v7.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45rav3t93v7>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45rav3t93v7>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45rav3t93v7>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45rav3t93v7>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw45rav3t93v7.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tatsu-section-background-wrap">
                                    <div class="tatsu-section-background"></div>
                                </div>
                                <style>
                                    .tatsu-g89jw45q3i7j4tlm {
                                        border-width: 0px 0px px 0px;
                                        border-style: solid;
                                    }

                                    .tatsu-g89jw45q3i7j4tlm .tatsu-section-pad {
                                        padding: 90px 0% 90px 0%;
                                    }

                                    .tatsu-g89jw45q3i7j4tlm>.tatsu-bottom-divider {
                                        z-index: 9999;
                                    }

                                    .tatsu-g89jw45q3i7j4tlm>.tatsu-top-divider {
                                        z-index: 9999;
                                    }
                                </style>
                            </div>
                            <div class="tatsu-g89jw45rggdtdh2r tatsu-section    tatsu-clearfix" data-title="" data-headerscheme="background--dark">
                                <div class='tatsu-section-pad clearfix' data-padding='{"d":"175px 0% 175px 0%"}' data-padding-top='175px'>
                                    <div class="tatsu-row-wrap  tatsu-wrap tatsu-row-one-col tatsu-row-has-one-cols tatsu-medium-gutter tatsu-reg-cols  tatsu-clearfix tatsu-g89jw45rib2sfspk">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-col tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45rl12ob5eu" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw45rn3emc8fb  ">
                                                                <div class="tatsu-text-inner   clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw45rn3emc8fb.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <h2 style="text-align: center;"><span style="color: #ffffff;">FALLEN IN <i class="font-icon icon-icon_heart_alt plain" style="color: #38d2d9;"></i> WITH ROOTS
                                                                            ?</span>
                                                                    </h2>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45rl12ob5eu.tatsu-column {
                                                        width: 100%;
                                                    }

                                                    .tatsu-g89jw45rl12ob5eu.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45rl12ob5eu>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45rl12ob5eu>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45rl12ob5eu>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45rl12ob5eu>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tatsu-section-background-wrap">
                                    <div class="tatsu-section-background"></div>
                                </div>
                                <style>
                                    .tatsu-g89jw45rggdtdh2r .tatsu-section-background {
                                        background-image: url(image/oshin-splash-main.jpg);
                                        background-repeat: no-repeat;
                                        background-attachment: fixed;
                                        background-position: center center;
                                        background-size: cover;
                                    }

                                    .tatsu-g89jw45rggdtdh2r .tatsu-bg-blur {
                                        background-repeat: no-repeat;
                                        background-attachment: fixed;
                                        background-position: center center;
                                        background-size: cover;
                                    }

                                    .tatsu-g89jw45rggdtdh2r {
                                        border-width: 0px 0px px 0px;
                                        border-style: solid;
                                    }

                                    .tatsu-g89jw45rggdtdh2r .tatsu-section-pad {
                                        padding: 175px 0% 175px 0%;
                                    }

                                    .tatsu-g89jw45rggdtdh2r>.tatsu-bottom-divider {
                                        z-index: 9999;
                                    }

                                    .tatsu-g89jw45rggdtdh2r>.tatsu-top-divider {
                                        z-index: 9999;
                                    }
                                </style>
                            </div>
                            <div id="why-oshin" class="tatsu-g89jw45rpe9nuvu0 tatsu-section    tatsu-clearfix" data-title="Why Oshin ?" data-headerscheme="background--dark">
                                <div class='tatsu-section-pad clearfix' data-padding='{"d":"90px 0% 90px 0%"}' data-padding-top='90px'>
                                    <div class="tatsu-row-wrap  tatsu-row-full-width tatsu-row-has-one-half tatsu-row-has-two-cols tatsu-zero-margin tatsu-no-gutter tatsu-eq-cols  tatsu-clearfix tatsu-g89jw45rrb80gbuk">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-half tatsu-column-align-middle tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45ru542ac3m" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw45rvy4dlh36  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw45rvy4dlh36.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <p><img class="alignnone size-full wp-image-10" src="image/oshin-demo-v1.png" alt="oshin-demo-v1" width="1160" height="1064" srcset="image/oshin-demo-v1.png 1160w, image/oshin-demo-v1-300x275.png 300w, image/oshin-demo-v1-1024x939.png 1024w, image/oshin-demo-v1-650x596.png 650w, image/oshin-demo-v1-1000x917.png 1000w" sizes="(max-width: 1160px) 100vw, 1160px" />
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45ru542ac3m.tatsu-column {
                                                        width: 50%;
                                                    }

                                                    .tatsu-g89jw45ru542ac3m.tatsu-column>.tatsu-column-inner>.tatsu-column-pad-wrap>.tatsu-column-pad {
                                                        padding: 0% 0% 5% 5%;
                                                    }

                                                    .tatsu-g89jw45ru542ac3m.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45ru542ac3m>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45ru542ac3m>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45ru542ac3m>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45ru542ac3m>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw45ru542ac3m.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-half tatsu-column-align-middle tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45ryf2gd7yw" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw45s06a2cxf9  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw45s06a2cxf9.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <h3>WHY ROOTS  ?</h3>
                                                                    <p><span style="font-size: 18px; line-height: 1.8;">
                                                                            High Quality Design, Super Slick Performance, Unique Layouts and a proven track record of successful themes and customer support sets us apart from the rest
</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="tatsu-empty-space  tatsu-g89jw45s1cau7fgh  ">
                                                                <style>
                                                                    .tatsu-g89jw45s1cau7fgh.tatsu-empty-space {
                                                                        height: 20px;
                                                                    }
                                                                </style>
                                                            </div>
                                                            <div class="tatsu-module tatsu-title-icon  tatsu-g89jw45s27gb4qd3  ">
                                                                <span class="tatsu-ti-wrap tatsu-ti medium circled align-left"><i class="icon-genius tatsu-ti tatsu-ti-icon"></i></span>
                                                                <div class="tatsu-tc tatsu-tc-custom-space align-left medium circled">
                                                                    <h6>VIDEO PRODUCTION
                                                                    </h6>
                                                                    <p>Delivering high production value for your video production is our mission. We are specialists for ads, social media videos. We have a committed and energetic approach, making the most of each project’s time & budgetary constraints</p>
                                                                </div>
                                                                <style>
                                                                    .tatsu-g89jw45s27gb4qd3 .tatsu-ti-wrap.circled {
                                                                        background: #fafbfd;
                                                                        box-shadow: 0px 0px 0px 0px rgba(0, 0, 0, 0);
                                                                    }

                                                                    .tatsu-g89jw45s27gb4qd3 .tatsu-ti-icon {
                                                                        color: #38d2d9;
                                                                    }
                                                                </style>
                                                            </div>
                                                            <!--<div class="tatsu-module tatsu-title-icon  tatsu-g89jw45s3l81uh4h  ">
                                                                <span class="tatsu-ti-wrap tatsu-ti medium circled align-left"><i class="icon-layers tatsu-ti tatsu-ti-icon"></i></span>
                                                                <div class="tatsu-tc tatsu-tc-custom-space align-left medium circled">
                                                                    <h6>UNIQUE LAYOUTS</h6>
                                                                    <p>Oshine comes with over 10 different header layout
                                                                        combinations, 5 menu layouts, 3 body layouts
                                                                        &#038;
                                                                        3 footer layouts, which you can combine in any
                                                                        manner.</p>
                                                                </div>
                                                                <style>
                                                                    .tatsu-g89jw45s3l81uh4h .tatsu-ti-wrap.circled {
                                                                        background: #fafbfd;
                                                                        box-shadow: 0px 0px 0px 0px rgba(0, 0, 0, 0);
                                                                    }

                                                                    .tatsu-g89jw45s3l81uh4h .tatsu-ti-icon {
                                                                        color: #38d2d9;
                                                                    }
                                                                </style>
                                                            </div>
                                                            <div class="tatsu-module tatsu-title-icon  tatsu-g89jw45s4yamji1i  ">
                                                                <span class="tatsu-ti-wrap tatsu-ti medium circled align-left"><i class="icon-pictures2 tatsu-ti tatsu-ti-icon"></i></span>
                                                                <div class="tatsu-tc tatsu-tc-custom-space align-left medium circled">
                                                                    <h6>UNLIMTED PORTFOLIOS</h6>
                                                                    <p>With Variable gutters, multiple columns, gradient
                                                                        overlays, 8 hover options, 7 Title styles,
                                                                        likeable
                                                                        items and many more options, Oshine literally
                                                                        provides unlimited ways to present your work.
                                                                    </p>
                                                                </div>
                                                                <style>
                                                                    .tatsu-g89jw45s4yamji1i .tatsu-ti-wrap.circled {
                                                                        background: #fafbfd;
                                                                        box-shadow: 0px 0px 0px 0px rgba(0, 0, 0, 0);
                                                                    }

                                                                    .tatsu-g89jw45s4yamji1i .tatsu-ti-icon {
                                                                        color: #38d2d9;
                                                                    }
                                                                </style>
                                                            </div>-->
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45ryf2gd7yw.tatsu-column {
                                                        width: 50%;
                                                    }

                                                    .tatsu-g89jw45ryf2gd7yw.tatsu-column>.tatsu-column-inner>.tatsu-column-pad-wrap>.tatsu-column-pad {
                                                        padding: 10% 20% 10% 10%;
                                                    }

                                                    .tatsu-g89jw45ryf2gd7yw.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45ryf2gd7yw>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45ryf2gd7yw>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45ryf2gd7yw>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45ryf2gd7yw>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw45ryf2gd7yw.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tatsu-section-background-wrap">
                                    <div class="tatsu-section-background"></div>
                                </div>
                                <style>
                                    .tatsu-g89jw45rpe9nuvu0 {
                                        border-width: 0px 0px px 0px;
                                        border-style: solid;
                                    }

                                    .tatsu-g89jw45rpe9nuvu0 .tatsu-section-pad {
                                        padding: 90px 0% 90px 0%;
                                    }

                                    .tatsu-g89jw45rpe9nuvu0>.tatsu-bottom-divider {
                                        z-index: 9999;
                                    }

                                    .tatsu-g89jw45rpe9nuvu0>.tatsu-top-divider {
                                        z-index: 9999;
                                    }
                                </style>
                            </div>
                            <div id="portfolio" class="tatsu-g89jw45s768u6zt7 tatsu-section    tatsu-clearfix" data-title="Portfolio" data-headerscheme="background--dark">
                                <div class='tatsu-section-pad clearfix' data-padding='{"d":"90px 0% 90px 0%"}' data-padding-top='90px'>
                                    <div class="tatsu-row-wrap  tatsu-wrap tatsu-row-one-col tatsu-row-has-one-cols tatsu-medium-gutter tatsu-reg-cols  tatsu-clearfix tatsu-g89jw45s99ay9320">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-col tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45sc15uvpd" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="special-heading-wrap style3  tatsu-g89jw45se410vujq oshine-module 1   ">
                                                                <div class="special-heading align-center">
                                                                    <h3 class="special-h-tag">OUR WORK</h3>
                                                                </div>
                                                                <div class="caption-wrap bottom-caption">
                                                                    <h6 class="caption special-subtitle">Recent Projects
                                                                        we have completed<span class="caption-inner"></span>
                                                                    </h6>
                                                                </div>
                                                                <style>
                                                                    .tatsu-g89jw45se410vujq .top-caption h6 {
                                                                        color: #999999;
                                                                        font-size: 16px;
                                                                    }

                                                                    .tatsu-g89jw45se410vujq .bottom-caption h6 {
                                                                        color: #999999;
                                                                        font-size: 20px;
                                                                    }

                                                                    .tatsu-g89jw45se410vujq.special-heading-wrap.style3 .top-caption .caption .caption-inner {
                                                                        background: #efefef;
                                                                    }

                                                                    .tatsu-g89jw45se410vujq.special-heading-wrap.style3 .bottom-caption .caption .caption-inner {
                                                                        background: #cccccc;
                                                                    }
                                                                </style>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw45sc15uvpd.tatsu-column {
                                                        width: 100%;
                                                    }

                                                    .tatsu-g89jw45sc15uvpd.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw45sc15uvpd>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45sc15uvpd>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45sc15uvpd>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw45sc15uvpd>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tatsu-row-wrap  tatsu-row-full-width tatsu-row-one-col tatsu-row-has-one-cols tatsu-medium-gutter tatsu-reg-cols  tatsu-clearfix tatsu-g89jw45sexa808pv">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-col tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw45shle5fpo2" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="portfolio-all-wrap oshine-portfolio-module tatsu-g89jw45skn9f4hh6">
                                                                <div class="portfolio portfolio-delay-load full-screen  portfolio-lazy-load full-screen-gutter masonry_disable style1-gutter four-col " data-animation="fadeInDown" data-action="get_ajax_full_screen_gutter_portfolio" data-category="brochures,classic,design,lightbox,mobile,photography,slider,videos,vertical-portfolio" data-aspect-ratio="1.6" data-enable-masonry="0" data-showposts="12" data-paged="2" data-col="four" data-gallery="0" data-filter="portfolio_categories" data-show_filters="no" data-thumbnail-bg-color="" data-thumbnail-bg-gradient="background:rgba(255,255,255,0.85)" data-title-style="style5" data-placeholder-color="rgba(255,255,255,1)" data-cat-color="#757575" data-title-color="#000000" data-gutter-width="50" data-img-grayscale="c_to_c" "
                                                                data-gradient-style-color=" background:rgba(255,255,255,0.85)" data-cat-hide="0" data-like-indicator="0" style="margin-right: 50px;">
                                                                <div class="portfolio-container clickable clearfix portfolio-shortcode  init-slide-top ">
                                                                    <div class="element be-hoverlay not-wide no-wide-width-height  c_to_c style5-title" style="margin-bottom: 50px;" data-category-names="">
                                                                        <div class="element-inner" style="margin-left: 50px;"><a href="#0" class=" thumb-wrap   " data-gdpr-atts={} title="iona-sam-2">
                                                                                <div class="flip-wrap">
                                                                                    <div style="padding-bottom : 62.5%;background-color:rgba(255,255,255,1);" class="flip-img-wrap"><img data-src="https://21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com/v14/wp-content/uploads/sites/16/2013/10/iona-sam-2-650x385.jpg" alt="" /></div>
                                                                                </div>
                                                                                <div class="thumb-overlay ">
                                                                                    <div class="thumb-bg " style="background:rgba(255,255,255,0.85)">
                                                                                        <div class="thumb-title-wrap ">
                                                                                            <div class="thumb-title " data-animation-type="none" style="color:#000000;text-align: center;">
                                                                                                Magazine Design
                                                                                            </div>
                                                                                            <div class="portfolio-item-cats " data-animation-type="none" style="color:#757575;text-align: center;">
                                                                                                <span>Classic</span><span>
                                                                                                        &middot;
                                                                                                    </span><span>Design</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            <div class="portfolio-like like-button-wrap">
                                                                                <a href="#" class="custom-like-button no-liked" data-post-id="2972"><i class="font-icon icon-icon_heart"></i><span>24</span></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="element be-hoverlay not-wide no-wide-width-height  c_to_c style5-title" style="margin-bottom: 50px;" data-category-names="">
                                                                        <div class="element-inner" style="margin-left: 50px;"><a href="#0" class=" thumb-wrap   " data-gdpr-atts={} title="oshin-model-0009">
                                                                                <div class="flip-wrap">
                                                                                    <div style="padding-bottom : 62.5%;background-color:rgba(255,255,255,1);" class="flip-img-wrap"><img data-src="image/oshin-model-0009-650x385.jpg" alt="" /></div>
                                                                                </div>
                                                                                <div class="thumb-overlay ">
                                                                                    <div class="thumb-bg " style="background:rgba(255,255,255,0.85)">
                                                                                        <div class="thumb-title-wrap ">
                                                                                            <div class="thumb-title " data-animation-type="none" style="color:#000000;text-align: center;">
                                                                                                Vertical Slider
                                                                                            </div>
                                                                                            <div class="portfolio-item-cats " data-animation-type="none" style="color:#757575;text-align: center;">
                                                                                                <span>Classic</span><span>
                                                                                                        &middot;
                                                                                                    </span><span>Design</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            <div class="portfolio-like like-button-wrap">
                                                                                <a href="#" class="custom-like-button no-liked" data-post-id="2971"><i class="font-icon icon-icon_heart"></i><span>8</span></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="element be-hoverlay not-wide no-wide-width-height  c_to_c style5-title" style="margin-bottom: 50px;" data-category-names="">
                                                                        <div class="element-inner" style="margin-left: 50px;"><a href="#0" class=" thumb-wrap   " data-gdpr-atts={} title="responsive-road">
                                                                                <div class="flip-wrap">
                                                                                    <div style="padding-bottom : 62.5%;background-color:rgba(255,255,255,1);" class="flip-img-wrap"><img data-src="image/responsive-road-650x385.jpg" alt="" /></div>
                                                                                </div>
                                                                                <div class="thumb-overlay ">
                                                                                    <div class="thumb-bg " style="background:rgba(255,255,255,0.85)">
                                                                                        <div class="thumb-title-wrap ">
                                                                                            <div class="thumb-title " data-animation-type="none" style="color:#000000;text-align: center;">
                                                                                                Headphones
                                                                                            </div>
                                                                                            <div class="portfolio-item-cats " data-animation-type="none" style="color:#757575;text-align: center;">
                                                                                                <span>Classic</span><span>
                                                                                                        &middot;
                                                                                                    </span><span>Design</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            <div class="portfolio-like like-button-wrap">
                                                                                <a href="#" class="custom-like-button no-liked" data-post-id="2969"><i class="font-icon icon-icon_heart"></i><span>8</span></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="element be-hoverlay not-wide no-wide-width-height  c_to_c style5-title" style="margin-bottom: 50px;" data-category-names="">
                                                                        <div class="element-inner" style="margin-left: 50px;"><a href="#0" class=" thumb-wrap   " data-gdpr-atts={} title="road-product">
                                                                                <div class="flip-wrap">
                                                                                    <div style="padding-bottom : 62.5%;background-color:rgba(255,255,255,1);" class="flip-img-wrap"><img data-src="image/road-product-650x385.jpg" alt="" /></div>
                                                                                </div>
                                                                                <div class="thumb-overlay ">
                                                                                    <div class="thumb-bg " style="background:rgba(255,255,255,0.85)">
                                                                                        <div class="thumb-title-wrap ">
                                                                                            <div class="thumb-title " data-animation-type="none" style="color:#000000;text-align: center;">
                                                                                                Left Fixed Sidebar
                                                                                            </div>
                                                                                            <div class="portfolio-item-cats " data-animation-type="none" style="color:#757575;text-align: center;">
                                                                                                <span>Classic</span><span>
                                                                                                        &middot;
                                                                                                    </span><span>Slider</span><span>
                                                                                                        &middot;
                                                                                                    </span><span>Videos</span><span>
                                                                                                        &middot;
                                                                                                    </span><span>Web</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            <div class="portfolio-like like-button-wrap">
                                                                                <a href="#" class="custom-like-button no-liked" data-post-id="1185"><i class="font-icon icon-icon_heart"></i><span>15</span></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="element be-hoverlay not-wide no-wide-width-height  c_to_c style5-title" style="margin-bottom: 50px;" data-category-names="">
                                                                        <div class="element-inner" style="margin-left: 50px;"><a href="#0" class=" thumb-wrap   " data-gdpr-atts={} title="iona-sam-2">
                                                                                <div class="flip-wrap">
                                                                                    <div style="padding-bottom : 62.5%;background-color:rgba(255,255,255,1);" class="flip-img-wrap"><img data-src="https://21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com/v14/wp-content/uploads/sites/16/2013/10/iona-sam-2-650x385.jpg" alt="" /></div>
                                                                                </div>
                                                                                <div class="thumb-overlay ">
                                                                                    <div class="thumb-bg " style="background:rgba(255,255,255,0.85)">
                                                                                        <div class="thumb-title-wrap ">
                                                                                            <div class="thumb-title " data-animation-type="none" style="color:#000000;text-align: center;">
                                                                                                Magazine Design
                                                                                            </div>
                                                                                            <div class="portfolio-item-cats " data-animation-type="none" style="color:#757575;text-align: center;">
                                                                                                <span>Classic</span><span>
                                                                                                        &middot;
                                                                                                    </span><span>Design</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            <div class="portfolio-like like-button-wrap">
                                                                                <a href="#" class="custom-like-button no-liked" data-post-id="2972"><i class="font-icon icon-icon_heart"></i><span>24</span></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="element be-hoverlay not-wide no-wide-width-height  c_to_c style5-title" style="margin-bottom: 50px;" data-category-names="">
                                                                        <div class="element-inner" style="margin-left: 50px;"><a href="#0" class=" thumb-wrap   " data-gdpr-atts={} title="oshin-model-0009">
                                                                                <div class="flip-wrap">
                                                                                    <div style="padding-bottom : 62.5%;background-color:rgba(255,255,255,1);" class="flip-img-wrap"><img data-src="image/oshin-model-0009-650x385.jpg" alt="" /></div>
                                                                                </div>
                                                                                <div class="thumb-overlay ">
                                                                                    <div class="thumb-bg " style="background:rgba(255,255,255,0.85)">
                                                                                        <div class="thumb-title-wrap ">
                                                                                            <div class="thumb-title " data-animation-type="none" style="color:#000000;text-align: center;">
                                                                                                Vertical Slider
                                                                                            </div>
                                                                                            <div class="portfolio-item-cats " data-animation-type="none" style="color:#757575;text-align: center;">
                                                                                                <span>Classic</span><span>
                                                                                                        &middot;
                                                                                                    </span><span>Design</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            <div class="portfolio-like like-button-wrap">
                                                                                <a href="#" class="custom-like-button no-liked" data-post-id="2971"><i class="font-icon icon-icon_heart"></i><span>8</span></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="element be-hoverlay not-wide no-wide-width-height  c_to_c style5-title" style="margin-bottom: 50px;" data-category-names="">
                                                                        <div class="element-inner" style="margin-left: 50px;"><a href="#0" class=" thumb-wrap   " data-gdpr-atts={} title="responsive-road">
                                                                                <div class="flip-wrap">
                                                                                    <div style="padding-bottom : 62.5%;background-color:rgba(255,255,255,1);" class="flip-img-wrap"><img data-src="image/responsive-road-650x385.jpg" alt="" /></div>
                                                                                </div>
                                                                                <div class="thumb-overlay ">
                                                                                    <div class="thumb-bg " style="background:rgba(255,255,255,0.85)">
                                                                                        <div class="thumb-title-wrap ">
                                                                                            <div class="thumb-title " data-animation-type="none" style="color:#000000;text-align: center;">
                                                                                                Headphones
                                                                                            </div>
                                                                                            <div class="portfolio-item-cats " data-animation-type="none" style="color:#757575;text-align: center;">
                                                                                                <span>Classic</span><span>
                                                                                                        &middot;
                                                                                                    </span><span>Design</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            <div class="portfolio-like like-button-wrap">
                                                                                <a href="#" class="custom-like-button no-liked" data-post-id="2969"><i class="font-icon icon-icon_heart"></i><span>8</span></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="element be-hoverlay not-wide no-wide-width-height  c_to_c style5-title" style="margin-bottom: 50px;" data-category-names="">
                                                                        <div class="element-inner" style="margin-left: 50px;"><a href="#0" class=" thumb-wrap   " data-gdpr-atts={} title="road-product">
                                                                                <div class="flip-wrap">
                                                                                    <div style="padding-bottom : 62.5%;background-color:rgba(255,255,255,1);" class="flip-img-wrap"><img data-src="image/road-product-650x385.jpg" alt="" /></div>
                                                                                </div>
                                                                                <div class="thumb-overlay ">
                                                                                    <div class="thumb-bg " style="background:rgba(255,255,255,0.85)">
                                                                                        <div class="thumb-title-wrap ">
                                                                                            <div class="thumb-title " data-animation-type="none" style="color:#000000;text-align: center;">
                                                                                                Left Fixed Sidebar
                                                                                            </div>
                                                                                            <div class="portfolio-item-cats " data-animation-type="none" style="color:#757575;text-align: center;">
                                                                                                <span>Classic</span><span>
                                                                                                        &middot;
                                                                                                    </span><span>Slider</span><span>
                                                                                                        &middot;
                                                                                                    </span><span>Videos</span><span>
                                                                                                        &middot;
                                                                                                    </span><span>Web</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </a>
                                                                            <div class="portfolio-like like-button-wrap">
                                                                                <a href="#" class="custom-like-button no-liked" data-post-id="1185"><i class="font-icon icon-icon_heart"></i><span>15</span></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tatsu-column-bg-image-wrap">
                                                    <div class="tatsu-column-bg-image"></div>
                                                </div>
                                            </div>
                                            <style>
                                                .tatsu-row>.tatsu-g89jw45shle5fpo2.tatsu-column {
                                                    width: 100%;
                                                }

                                                .tatsu-g89jw45shle5fpo2.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                    mix-blend-mode: none;
                                                }

                                                .tatsu-g89jw45shle5fpo2>.tatsu-column-inner>.tatsu-top-divider {
                                                    z-index: 9999;
                                                }

                                                .tatsu-g89jw45shle5fpo2>.tatsu-column-inner>.tatsu-bottom-divider {
                                                    z-index: 9999;
                                                }

                                                .tatsu-g89jw45shle5fpo2>.tatsu-column-inner>.tatsu-left-divider {
                                                    z-index: 9999;
                                                }

                                                .tatsu-g89jw45shle5fpo2>.tatsu-column-inner>.tatsu-right-divider {
                                                    z-index: 9999;
                                                }
                                            </style>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tatsu-section-background-wrap">
                                <div class="tatsu-section-background"></div>
                            </div>
                            <style>
                                .tatsu-g89jw45s768u6zt7.tatsu-section {
                                    background-color: #f2f3f8;
                                }

                                .tatsu-g89jw45s768u6zt7 {
                                    border-width: 0px 0px px 0px;
                                    border-style: solid;
                                }

                                .tatsu-g89jw45s768u6zt7 .tatsu-section-pad {
                                    padding: 90px 0% 90px 0%;
                                }

                                .tatsu-g89jw45s768u6zt7>.tatsu-bottom-divider {
                                    z-index: 9999;
                                }

                                .tatsu-g89jw45s768u6zt7>.tatsu-top-divider {
                                    z-index: 9999;
                                }
                            </style>
                        </div>
                        <div id="contact" class="tatsu-g89jw47vdh7c1abx tatsu-section    tatsu-clearfix" data-title="Contact" data-headerscheme="background--dark">
                                <div class='tatsu-section-pad clearfix' data-padding='{"d":"90px 0% 90px 0%"}' data-padding-top='90px'>
                                    <div class="tatsu-row-wrap  tatsu-wrap tatsu-row-has-one-half tatsu-row-has-two-cols tatsu-medium-gutter tatsu-reg-cols  tatsu-clearfix tatsu-g89jw47vgq76t9dm">
                                        <div class="tatsu-row ">
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-half tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw47vl1nxpjf" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw47vnv3ajipz  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw47vnv3ajipz.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <p><img class="alignnone size-full wp-image-3356" src="image/rootsCopy.png" alt="oshine-white-wo-border" width="85" height="11" srcset="image/rootsCopy.png 85w, image/oshine-white-wo-border-75x11.png 75w" sizes="(max-width: 85px) 100vw, 85px" /></p>
                                                                    <p><span style="color: #999999;">We’re passionate about delivering quality and effective online marketing solutions with amazing results
</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <ul class="tatsu-module tatsu-list tatsu-g89jw47vpl9hz8fv    tatsu-list-vertical-align-center tatsu-lists-icon">
                                                                <style>
                                                                    .tatsu-g89jw47vpl9hz8fv .tatsu-list-content {
                                                                        margin: 0 0 12px 0px;
                                                                    }

                                                                    .tatsu-g89jw47vpl9hz8fv .tatsu-list-content::before,
                                                                    .tatsu-g89jw47vpl9hz8fv .tatsu-icon {
                                                                        color: rgba(34, 147, 215, 1);
                                                                    }
                                                                </style>
                                                                <li class="tatsu-list-content tatsu-g89jw47vs4eevcj8   ">

                                                                    <style>
                                                                        .tatsu-g89jw47vs4eevcj8 .tatsu-icon,
                                                                        .tatsu-g89jw47vs4eevcj8.tatsu-list-content::before {
                                                                            color: #ffffff;
                                                                        }

                                                                        .tatsu-g89jw47vs4eevcj8 {
                                                                            border-style: solid;
                                                                        }
                                                                    </style>
                                                                </li>
                                                                <li class="tatsu-list-content tatsu-g89jw47vto8ay77q   ">
                                                                    <span class="tatsu-list-icon-wrap"><i class="tatsu-icon icon-icon_phone "></i></span><span class="tatsu-list-inner">
                                                                        <p><span style="color: #999999;font-size: 16px">
                                                                            <a href="tel:01112063179" class="" target="_blank">+01112063179</a>
                                                                            </span>
                                                                        </p>
                                                                    </span>
                                                                    <style>
                                                                        .tatsu-g89jw47vto8ay77q .tatsu-icon,
                                                                        .tatsu-g89jw47vto8ay77q.tatsu-list-content::before {
                                                                            color: #ffffff;
                                                                        }

                                                                        .tatsu-g89jw47vto8ay77q {
                                                                            border-style: solid;
                                                                        }
                                                                    </style>
                                                                </li>
                                                                <li class="tatsu-list-content tatsu-g89jw47vv636crgd   ">
                                                                    <span class="tatsu-list-icon-wrap"><i class="tatsu-icon icon-icon_mail_alt "></i></span><span class="tatsu-list-inner">
                                                                        <p><span style="color: #999999;"><a href="mailto:mohamedsayed130@outlook.com"><span class="__cf_email__" data-cfemail="067571676b6f466474676862637e766968636872752865696b"></span>mohamedsayed130@outlook.com</a></span>
                                                                        </p>
                                                                    </span>
                                                                    <style>
                                                                        .tatsu-g89jw47vv636crgd .tatsu-icon,
                                                                        .tatsu-g89jw47vv636crgd.tatsu-list-content::before {
                                                                            color: #ffffff;
                                                                        }

                                                                        .tatsu-g89jw47vv636crgd {
                                                                            border-style: solid;
                                                                        }
                                                                    </style>
                                                                </li>
                                                            </ul>
                                                            <div class="tatsu-empty-space  tatsu-g89jw47vwq13y9bd  ">
                                                                <style>
                                                                    .tatsu-g89jw47vwq13y9bd.tatsu-empty-space {
                                                                        height: 30px;
                                                                    }
                                                                </style>
                                                            </div>
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw47vxvcv62pb  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw47vxvcv62pb.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <h6>
                                                                        <span style="color: #ffffff; font-size: 13px;">FOLLOW
                                                                            US</span>
                                                                    </h6>
                                                                </div>
                                                            </div>
                                                            <div class="tatsu-module tatsu-normal-icon tatsu-icon-shortcode align-none tatsu-g89jw47w0sb6u20m   ">
                                                                <style>
                                                                    .tatsu-g89jw47w0sb6u20m .tatsu-icon {
                                                                        background-color: #38d2d9;
                                                                        color: #ffffff;
                                                                        border-color: #000000;
                                                                    }

                                                                    .tatsu-g89jw47w0sb6u20m .tatsu-icon:hover {
                                                                        background-color: #000000;
                                                                        color: #ffffff;
                                                                        border-color: #38d2d9;
                                                                    }

                                                                    .tatsu-g89jw47w0sb6u20m {
                                                                        border-width: 0px 0px 0px 0px;
                                                                    }
                                                                </style>
                                                                <a href="#" class="tatsu-icon-wrap circle     " aria-label="icon-facebook2" data-gdpr-atts={}><i class="tatsu-icon tatsu-custom-icon tatsu-custom-icon-class icon-facebook2 small circle" data-animation="none" data-animation-delay="0"></i></a>
                                                            </div>
                                                            <div class="tatsu-module tatsu-normal-icon tatsu-icon-shortcode align-none tatsu-g89jw47w2o4yid7c   ">
                                                                <style>
                                                                    .tatsu-g89jw47w2o4yid7c .tatsu-icon {
                                                                        background-color: #38d2d9;
                                                                        color: #ffffff;
                                                                        border-color: #000000;
                                                                    }

                                                                    .tatsu-g89jw47w2o4yid7c .tatsu-icon:hover {
                                                                        background-color: #000000;
                                                                        color: #ffffff;
                                                                        border-color: #38d2d9;
                                                                    }

                                                                    .tatsu-g89jw47w2o4yid7c {
                                                                        border-width: 0px 0px 0px 0px;
                                                                    }
                                                                </style>
                                                                <a href="#" class="tatsu-icon-wrap circle     " aria-label="icon-twitter2" data-gdpr-atts={}><i class="tatsu-icon tatsu-custom-icon tatsu-custom-icon-class icon-twitter2 small circle" data-animation="none" data-animation-delay="0"></i></a>
                                                            </div>
                                                            <div class="tatsu-module tatsu-normal-icon tatsu-icon-shortcode align-none tatsu-g89jw47w5v6anhmp   ">
                                                                <style>
                                                                    .tatsu-g89jw47w5v6anhmp .tatsu-icon {
                                                                        background-color: #38d2d9;
                                                                        color: #ffffff;
                                                                        border-color: #000000;
                                                                    }

                                                                    .tatsu-g89jw47w5v6anhmp .tatsu-icon:hover {
                                                                        background-color: #000000;
                                                                        color: #ffffff;
                                                                        border-color: #38d2d9;
                                                                    }

                                                                    .tatsu-g89jw47w5v6anhmp {
                                                                        border-width: 0px 0px 0px 0px;
                                                                    }
                                                                </style>
                                                                <a href="#" class="tatsu-icon-wrap circle     " aria-label="icon-social_dribbble" data-gdpr-atts={}><i class="tatsu-icon tatsu-custom-icon tatsu-custom-icon-class icon-social_dribbble small circle" data-animation="none" data-animation-delay="0"></i></a>
                                                            </div>
                                                            <div class="tatsu-module tatsu-normal-icon tatsu-icon-shortcode align-none tatsu-g89jw47w7q3p93w1   ">
                                                                <style>
                                                                    .tatsu-g89jw47w7q3p93w1 .tatsu-icon {
                                                                        background-color: #38d2d9;
                                                                        color: #ffffff;
                                                                        border-color: #000000;
                                                                    }

                                                                    .tatsu-g89jw47w7q3p93w1 .tatsu-icon:hover {
                                                                        background-color: #000000;
                                                                        color: #ffffff;
                                                                        border-color: #38d2d9;
                                                                    }

                                                                    .tatsu-g89jw47w7q3p93w1 {
                                                                        border-width: 0px 0px 0px 0px;
                                                                    }
                                                                </style>
                                                                <a href="#" class="tatsu-icon-wrap circle     " aria-label="icon-social_pinterest" data-gdpr-atts={}><i class="tatsu-icon tatsu-custom-icon tatsu-custom-icon-class icon-social_pinterest small circle" data-animation="none" data-animation-delay="0"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw47vl1nxpjf.tatsu-column {
                                                        width: 50%;
                                                    }

                                                    .tatsu-g89jw47vl1nxpjf.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw47vl1nxpjf>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47vl1nxpjf>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47vl1nxpjf>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47vl1nxpjf>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw47vl1nxpjf.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div class="tatsu-column  tatsu-column-no-bg tatsu-one-half tatsu-column-image-none tatsu-column-effect-none  tatsu-g89jw47wah64tyt6" data-parallax-speed="0" style="">
                                                <div class="tatsu-column-inner ">
                                                    <div class="tatsu-column-pad-wrap">
                                                        <div class="tatsu-column-pad">
                                                            <div class="tatsu-module tatsu-text-block-wrap tatsu-g89jw47wdjbzmq26  ">
                                                                <div class="tatsu-text-inner tatsu-align-center  clearfix">
                                                                    <style>
                                                                        .tatsu-g89jw47wdjbzmq26.tatsu-text-block-wrap .tatsu-text-inner {
                                                                            text-align: left;
                                                                        }
                                                                    </style>
                                                                    <h6><span style="color: #ffffff;">CONTACT
                                                                            US</span>
                                                                    </h6>
                                                                    <p><span style="color: #999999;">We love to listen
                                                                            and we are eagerly waiting to talk to you
                                                                            regarding your project. Get in touch with us
                                                                            if you have any queries and we will get back
                                                                            to you as soon as possible.</span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                           
                                                            <div class="contact_form contact_form_module oshine-module style1 style1-input tatsu-g89jw47wgc6484q1   " data-consent-error="Please check the consent box, in order for us to process this form">
                                                                <?php
                                                                if (isset($_SESSION['flash'])) {
                                                                    echo"
                                                                    <h5 class='contact_form_3'>". $_SESSION['flash'];
                                    unset($_SESSION['flash']);
                                    "

                            ";
                                                                }
                                                                ?>


                                                                <form method="post" action="#contact" class="contact">
                                                                    <input type="text" name="name" class="txt autoclear" placeholder="Name" style="border-width:2px" />

                                                                    <input type="text" name="email" class="txt autoclear" placeholder="Email" style="border-width:2px" />

                                                                    <input type="text" name="subject" class="txt autoclear" placeholder="Subject" style="border-width:2px" />

                                                                    <textarea name="message" class="txt_area autoclear" placeholder="Message" style="border-width:2px"></textarea>

                                                                    <input type="submit" name="b_reg" value="Submit" />

                                                                </form>
                                                                <style>
                                                                    .tatsu-g89jw47wgc6484q1 input[type="text"],
                                                                    .tatsu-g89jw47wgc6484q1 textarea {
                                                                        background-color: transparent;
                                                                        color: #909090;
                                                                        border-color: #323232;
                                                                    }

                                                                    .tatsu-g89jw47wgc6484q1 input[type="text"] {
                                                                        height: 40px;
                                                                    }

                                                                    .tatsu-g89jw47wgc6484q1 .contact_submit {
                                                                        background-color: #38d2d9;
                                                                    }
                                                                </style>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tatsu-column-bg-image-wrap">
                                                        <div class="tatsu-column-bg-image"></div>
                                                    </div>
                                                </div>
                                                <style>
                                                    .tatsu-row>.tatsu-g89jw47wah64tyt6.tatsu-column {
                                                        width: 50%;
                                                    }

                                                    .tatsu-g89jw47wah64tyt6.tatsu-column>.tatsu-column-inner>.tatsu-column-overlay {
                                                        mix-blend-mode: none;
                                                    }

                                                    .tatsu-g89jw47wah64tyt6>.tatsu-column-inner>.tatsu-top-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47wah64tyt6>.tatsu-column-inner>.tatsu-bottom-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47wah64tyt6>.tatsu-column-inner>.tatsu-left-divider {
                                                        z-index: 9999;
                                                    }

                                                    .tatsu-g89jw47wah64tyt6>.tatsu-column-inner>.tatsu-right-divider {
                                                        z-index: 9999;
                                                    }

                                                    @media only screen and (max-width: 767px) {
                                                        .tatsu-row>.tatsu-g89jw47wah64tyt6.tatsu-column {
                                                            width: 100%;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tatsu-section-background-wrap">
                                    <div class="tatsu-section-background"></div>
                                </div>
                                <style>
                                    .tatsu-g89jw47vdh7c1abx.tatsu-section {
                                        background-color: #161616;
                                    }

                                    .tatsu-g89jw47vdh7c1abx {
                                        border-width: 0px 0px px 0px;
                                        border-style: solid;
                                    }

                                    .tatsu-g89jw47vdh7c1abx .tatsu-section-pad {
                                        padding: 90px 0% 90px 0%;
                                    }

                                    .tatsu-g89jw47vdh7c1abx>.tatsu-bottom-divider {
                                        z-index: 9999;
                                    }

                                    .tatsu-g89jw47vdh7c1abx>.tatsu-top-divider {
                                        z-index: 9999;
                                    }
                                </style>
                            </div>
                        </div> <!--  End Page Content -->
                    </section>
                </div>
            </div>
            <footer id="footer" class="layout-wide">
                <span class="footer-border be-wrap "></span>
                <div id="footer-wrap" class=" style1 be-wrap clearfix">

                    <div class="footer-left-area">
                    </div>

                    <div class="footer-center-area">
                        <div class="footer-content-inner-center">
                            Copyright OmarTarek Exponents 2020. All Rights Reserved
                        </div>
                    </div>

                    <div class="footer-right-area">

                    </div>
                </div>
            </footer>
        </div>
        <div class="loader page-loader">
            <div class="loader-style1-wrap">
                <div class="loader-style1-double-bounce1"></div>
                <div class="loader-style1-double-bounce2"></div>
            </div>
        </div>
        <a href="#" id="back-to-top" class="layout-wide"><i class="font-icon icon-arrow_carrot-up"></i></a>
    </div>


    <input type="hidden" id="ajax_url" value="https://oshinewptheme.com/v14/wp-admin/admin-ajax.php" />

    <div id="gallery" class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="pswp__bg"></div>

        <div class="pswp__scroll-wrap">

            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <div class="pswp__ui pswp__ui--hidden">

                <div class="pswp__top-bar">

                    <div class="pswp__counter"></div>

                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                    <button class="pswp__button pswp__button--share" title="Share"></button>

                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- <div class="pswp__loading-indicator"><div class="pswp__loading-indicator__line"></div></div> -->

                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip">
                        <!-- <a href="#" class="pswp__share--facebook"></a>
                    <a href="#" class="pswp__share--twitter"></a>
                    <a href="#" class="pswp__share--pinterest"></a>
                    <a href="#" download class="pswp__share--download"></a> -->
                    </div>
                </div>

                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
                <div class="pswp__caption">
                    <div class="pswp__caption__center">
                    </div>
                </div>
            </div>

        </div>

    </div>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:700%2C400%7CRaleway:300" rel="stylesheet" property="stylesheet" media="all" type="text/css">

    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script type="text/javascript">
        if (typeof revslider_showDoubleJqueryError === "undefined") {
            function revslider_showDoubleJqueryError(sliderID) {
                var err = "<div class='rs_error_message_box'>";
                err += "<div class='rs_error_message_oops'>Oops...</div>";
                err += "<div class='rs_error_message_content'>";
                err +=
                    "You have some jquery.js library include that comes after the Slider Revolution files js inclusion.<br>";
                err +=
                    "To fix this, you can:<br>&nbsp;&nbsp;&nbsp; 1. Set 'Module General Options' ->  'jQuery & OutPut Filters' -> 'Put JS to Body' to on";
                err += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jQuery.js inclusion and remove it";
                err += "</div>";
                err += "</div>";
                jQuery(sliderID).show().html(err);
            }
        }
    </script>
    <script type='text/javascript' src='js/comment-reply.min.js'>
    </script>
    <script type='text/javascript' src='js/asyncloader.min.js'>
    </script>
    <script type='text/javascript' src='js/core.min.js'>
    </script>
    <script type='text/javascript' src='js/widget.min.js'>
    </script>
    <script type='text/javascript' src='js/accordion.min.js'>
    </script>
    <script type='text/javascript' src='js/tabs.min.js'>
    </script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var oshineModulesConfig = {
            "pluginUrl": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/",
            "vendorScriptsUrl": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/",
            "dependencies": {
                "asyncloader": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/asyncloader.min.js",
                "backgroundcheck": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/backgroundcheck.min.js",
                "backgroundposition": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/backgroundposition.min.js",
                "be-modules-plugin": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/be-modules-plugin.min.js",
                "beslider": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/beslider.min.js",
                "countdown": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/countdown.min.js",
                "easing": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/easing.min.js",
                "easypiechart": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/easypiechart.min.js",
                "fitvids": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/fitvids.min.js",
                "fullscreenheight": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/fullscreenheight.min.js",
                "hoverdir": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/hoverdir.min.js",
                "imagesloaded": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/imagesloaded.min.js",
                "isotope": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/isotope.min.js",
                "justifiedgallery": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/justifiedgallery.min.js",
                "magnificpopup": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/magnificpopup.min.js",
                "mousewheel": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/mousewheel.min.js",
                "owlcarousel": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/owlcarousel.min.js",
                "photoswipe": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/photoswipe.min.js",
                "resizetoparent": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/resizetoparent.min.js",
                "rotate": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/rotate.min.js",
                "tilt": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/tilt.min.js",
                "typed": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/typed.min.js",
                "vivusSVGanimation": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/vivusSVGanimation.min.js",
                "waypoints": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/oshine-modules\/public\/js\/vendor\/waypoints.min.js"
            }
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='js/oshine-modules.min.js'>
    </script>
    <script type='text/javascript' src='js/es6-promise.auto.min.js'>
    </script>
    <script type='text/javascript' src='js/helpers.min.js'>
    </script>
    <script type='text/javascript' src='js/debouncedresize.min.js'>
    </script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var tatsuFrontendConfig = {
            "pluginUrl": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu",
            "vendorScriptsUrl": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/",
            "mapsApiKey": "AIzaSyCmcsMuS830hpPUQnRNB-NsiYoHDt8sH8o",
            "dependencies": {
                "anime": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/anime.min.js?ver=3.2",
                "asyncloader": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/asyncloader.min.js?ver=3.2",
                "backgroundposition": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/backgroundposition.min.js?ver=3.2",
                "begrid": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/begrid.min.js?ver=3.2",
                "countTo": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/countTo.min.js?ver=3.2",
                "debouncedresize": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/debouncedresize.min.js?ver=3.2",
                "es6-promise.auto": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/es6-promise.auto.min.js?ver=3.2",
                "fitvids": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/fitvids.min.js?ver=3.2",
                "flickity": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/flickity.min.js?ver=3.2",
                "hoverintent": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/hoverintent.min.js?ver=3.2",
                "imagesloaded": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/imagesloaded.min.js?ver=3.2",
                "isotope": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/isotope.min.js?ver=3.2",
                "magnificpopup": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/magnificpopup.min.js?ver=3.2",
                "stickykit": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/stickykit.min.js?ver=3.2",
                "superfish": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/superfish.min.js?ver=3.2",
                "tatsuCarousel": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/tatsuCarousel.min.js?ver=3.2",
                "tatsuColumnParallax": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/tatsuColumnParallax.min.js?ver=3.2",
                "tatsuParallax": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/tatsuParallax.min.js?ver=3.2",
                "tilt": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/tilt.min.js?ver=3.2",
                "typed": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/typed.min.js?ver=3.2",
                "unveil": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/unveil.min.js?ver=3.2",
                "vivus": "https:\/\/oshinewptheme.com\/v14\/wp-content\/plugins\/tatsu\/public\/js\/vendor\/vivus.min.js?ver=3.2"
            },
            "slider_icons": {
                "left": "<svg viewBox=\"0 0 10 16\" fill=\"none\" xmlns=\"http:\/\/www.w3.org\/2000\/svg\">\n<path opacity=\"0.999943\" d=\"M0 0L7 7.1759L0.343184 14\" transform=\"translate(9 15) rotate(-180)\" stroke-width=\"2\"\/>\n<\/svg>\n",
                "right": "<svg viewBox=\"0 0 10 16\" fill=\"none\" xmlns=\"http:\/\/www.w3.org\/2000\/svg\">\n<path d=\"M0 0L7 7.1759L0.343184 14\" transform=\"translate(1 1)\" stroke-width=\"2\"\/>\n<\/svg>\n"
            },
            "version": "3.2"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='js/tatsu.min.js'>
    </script>
    <script type='text/javascript' src='js/perfect-scrollbar.jquery.min.js'>
    </script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var oshineThemeConfig = {
            "vendorScriptsUrl": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/",
            "dependencies": {
                "asyncloader": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/asyncloader.min.js",
                "backgroundcheck": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/backgroundcheck.min.js",
                "backgroundposition": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/backgroundposition.min.js",
                "classie": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/classie.min.js",
                "easing": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/easing.min.js",
                "fitvids": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/fitvids.min.js",
                "flickity": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/flickity.min.js",
                "fullscreenheight": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/fullscreenheight.min.js",
                "galaxycanvas": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/galaxycanvas.min.js",
                "greensock": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/greensock.min.js",
                "horizontalcarousel": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/horizontalcarousel.min.js",
                "hoverintent": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/hoverintent.min.js",
                "imagesloaded": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/imagesloaded.min.js",
                "isotope": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/isotope.min.js",
                "magnificpopup": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/magnificpopup.min.js",
                "mcustomscrollbar": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/mcustomscrollbar.min.js",
                "modernizr": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/modernizr.min.js",
                "mousewheel": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/mousewheel.min.js",
                "multi_level_menu": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/multi_level_menu.min.js",
                "old_menu_animation": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/old_menu_animation.min.js",
                "page_stack_top": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/page_stack_top.min.js",
                "patterncanvas": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/patterncanvas.min.js",
                "perfect-scrollbar.jquery": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/perfect-scrollbar.jquery.min.js",
                "perspective_navigation": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/perspective_navigation.min.js",
                "request_animation_frame": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/request_animation_frame.min.js",
                "resizetoparent": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/resizetoparent.min.js",
                "scrolltosections": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/scrolltosections.min.js",
                "simplebar": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/simplebar.min.js",
                "simplebar_old": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/simplebar_old.min.js",
                "sticky_sections": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/sticky_sections.min.js",
                "stickykit": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/stickykit.min.js",
                "superfish": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/superfish.min.js",
                "transparentheader": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/transparentheader.min.js",
                "waterdropcanvas": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/waterdropcanvas.min.js",
                "webfont": "https:\/\/21g6p436jemo16mo9n1a25yq-wpengine.netdna-ssl.com\/wp-content\/themes\/oshin\/js\/vendor\/webfont.min.js"
            }
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='js/script.min.js'>
    </script>
    <script type='text/javascript' src='js/wp-embed.min.js'>
    </script>
    <!-- Option Panel Custom JavaScript -->
    <script>
        //jQuery(document).ready(function(){
        // });
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>

</html>